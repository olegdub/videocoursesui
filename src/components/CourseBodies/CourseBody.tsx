import { FC } from "react"
import { ICourseBody } from "../../data/types"
import { Box, Button, Flex, Heading, Icon, List, ListIcon, ListItem, Text } from "@chakra-ui/react"
import { supportedBodies, supportedIcons } from "../../data/courses"
import { CheckCircleIcon, CheckIcon, EmailIcon } from "@chakra-ui/icons"
import { colors } from "../../theme"
import { Link } from "react-router-dom"
import MDEditor from "@uiw/react-md-editor"



interface ICourseBodyProps {
    body: ICourseBody,
    courseId: number
}

const CourseBody: FC<ICourseBodyProps> = ({body, courseId}): JSX.Element => {

    const bodyItems = body.items.map( it => {

        return (
            <MDEditor.Markdown key={it.id} source={it.text} />
        )
    })

    if (body.isAction) {
        return (
            <Box
                p='40px'
                borderRadius='25px'
                boxShadow='0px 4px 20px rgba(51, 51, 51, 0.1)'
            >
                <Heading
                    as='h2'
                    fontSize='24px'
                >
                    Курс включает в себя:
                </Heading>

                <Flex
                    flexDir='column'
                >   
                    <List
                        my='20px'
                    >
                        {bodyItems}
                    </List>

                    <Link to={`/buy/${courseId}`}>
                        <Box
                            w='100%'
                            borderRadius='25px'
                            p='10px 30px'
                            textAlign='center'
                            textTransform='capitalize'
                            color='White'
                            bg={colors.gradient}
                            mb='20px'
                            _hover={{opacity: '0.6'}}
                        >
                            Перейти к курсу
                        </Box>
                    </Link>

                    <Box
                        textDecor='underline'
                        cursor='pointer'
                        _hover={{textDecor: 'none', cursor: 'pointer'}}
                    >
                        <Link to={`/gift/${courseId}`}>
                            <Text
                                my='10px'
                                display='flex'
                                alignItems='center'
                                lineHeight='150%'
                                gap='15px'
                                
                            >
                                <Icon
                                    // @ts-ignore
                                    as={EmailIcon} 
                                    color={colors.control}
                                    w='32px'
                                    h='32px'

                                />
                                    купить в подарок
                            </Text>
                        </Link>
                    </Box>
                    
                    

                </Flex>
            </Box>
        )
    }

    return (
        <Box
            bg={body.bodyStyles.bg}
            border={body.bodyStyles.border}
            borderColor='gray.300'
            p={body.bodyStyles.padding}
            borderRadius='25px'
        >
            <Heading
                as='h2'
                fontSize='24px'
            >
                {body.title}
            </Heading>

            <Flex
                flexDir='column'
            >   
                <List>
                    {bodyItems}
                </List>
            </Flex>

        </Box>
    )
}


export default CourseBody