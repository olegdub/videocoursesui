import { AccordionButton, AccordionIcon, AccordionItem, AccordionPanel, Box } from "@chakra-ui/react"
import { FC } from "react"

export interface IAccordionItemData {
    title: string,
    text: string
}

export interface IMyAccordionItemProps {
    props: IAccordionItemData
}


const MyAccordionItem: FC<IMyAccordionItemProps> = ({props}): JSX.Element => {

    return (
        <AccordionItem p='10px 0'>
            <h5>
            <AccordionButton p='10px 0'>
                <Box as="span" flex='1' textAlign='left'>
                    {props.title}
                </Box>
                <AccordionIcon ml='15px' />
            </AccordionButton>
            </h5>
            <AccordionPanel pb={4}>
                {props.text}
            </AccordionPanel>
        </AccordionItem>
    )
}

export default MyAccordionItem