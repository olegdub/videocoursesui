/* eslint-disable jsx-a11y/label-has-associated-control */
import { useEffect, useState } from 'react'
import { Box, Container, Flex, Heading, Spinner, Text } from "@chakra-ui/react";
import {  Navigate  } from "react-router-dom";
import LoginForm from '../Forms/LoginForm';
import { useAppDispach, useAppSelector } from '../../hooks/redux';
import { login } from '../../redux/actions/authAction';
import { IHandleLogin } from '../../types/auth';
import { sitePaths } from '../../axios';


function Login() {
  const dispatch = useAppDispach()
  const {isAuthenticated, loading, error } = useAppSelector(state => state.auth)

  // // временный стейт
  // const [resMessage, setResMessage] = useState<string>("")

  // обработка отправки формы
  const onLogin = ({ isSuccessLogin, formValue }: IHandleLogin) => {
    if (isSuccessLogin) {
      dispatch(login(formValue))
    }
  }

  // useEffect(() => {
  //   setResMessage("")
    
  // }, [isAuthenticated, setResMessage])

  /* если пользователь авторизирован редирект на главную */
  if (isAuthenticated && !loading){
    return <Navigate to={sitePaths.userCourses}/>
  }

  return (
    <Box>
        {<LoginForm handleLogin={onLogin} resMessage={error}/>}
    </Box>
  )

}

export default Login