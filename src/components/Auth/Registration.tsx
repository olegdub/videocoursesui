/* eslint-disable jsx-a11y/label-has-associated-control */
import { useEffect, useState } from 'react'
import { Box} from "@chakra-ui/react";
import {  Navigate  } from "react-router-dom";
import { useAppDispach, useAppSelector } from '../../hooks/redux';
import { IHandleRegister } from '../../types/auth';
import RegistrationForm from '../Forms/RegistrationForm';
import { registration } from '../../redux/actions/authAction';


function Registration() {
  const dispatch = useAppDispach()
  const {isAuthenticated, loading, error } = useAppSelector(state => state.auth)

  // временный стейт
  // const [resMessage, setResMessage] = useState<string>("")

  // обработка отправки формы
  const onSubmit = ({ isSuccess, formValue }: IHandleRegister) => {
    if (isSuccess) {
      dispatch(registration(formValue))
    }
  }

  // useEffect(() => {
  //   setResMessage("")
    
  // }, [isAuthenticated, setResMessage])

  /* если пользователь авторизирован редирект на главную */
  if (isAuthenticated && !loading){
    return <Navigate to='/'/>
  }

  return (
    <Box>
        {<RegistrationForm handle={onSubmit} resMessage={error}/>}
    </Box>
  )

}

export default Registration