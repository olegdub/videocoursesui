import { Navigate } from "react-router-dom";
import { useAppDispach } from "../../hooks/redux";
import { logout } from "../../redux/actions/authAction";
import { sitePaths } from "../../axios";


function LogOut() {
    const dispatch = useAppDispach()

    dispatch(logout())

    return (
        <Navigate to={sitePaths.home}/>
    )
}

export default LogOut;