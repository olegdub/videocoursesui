import { Box, Flex, FormLabel } from "@chakra-ui/react";
import { FC } from "react";
import MyPopover from "../MyPopover/MyPopover";
import { InfoIcon } from "@chakra-ui/icons";


export interface ILabelWithPopover {
    title: string,
    popover: {
        headerText: string,
        bodyText: string
    }
}

interface ILabelWithPopoverProps {
    props: ILabelWithPopover
}

const LabelWithPopover: FC<ILabelWithPopoverProps> = ({props}) => {

    return (
        <Flex
                 maxW='25%'
                 flex='1 0 25%'
                 pt='10px'
                 flexDir='column'
                 alignItems='flex-end'
             >
                 <FormLabel htmlFor="email" fontSize='16px' p='0' m='0'>
                    {props.title}
                 </FormLabel>

                 
                     <MyPopover
                         data={props.popover}
                     >
                         <Box
                             display='flex'
                             cursor='pointer'
                             alignItems='center'
                             gap='5px'
                             color='cyan.400'
                             fontSize='14px'
                             lineHeight='120%'
                             mt='4px'
                         >
                             Пример
                             <InfoIcon color='cyan.400' h='14px' w='14px'/>
                         </Box>
                     </MyPopover>
                     
                 
             </Flex>
    )
}

export default LabelWithPopover