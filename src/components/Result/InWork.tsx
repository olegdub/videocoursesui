import { Box, Heading, Text } from '@chakra-ui/react';
import { WarningTwoIcon } from '@chakra-ui/icons';

export default function InWork() {
  return (
    <Box textAlign="center" py={10} px={6}>
      <WarningTwoIcon boxSize={'50px'} color={'orange.300'} />
      <Heading as="h2" size="xl" mt={6} mb={2}>
        Страница находится в разработке
      </Heading>
      <Text color={'gray.500'}>
        Скоро здесь появится что-то крутое, 
        наберитесь терпения и возвращайтесь через некоторое время.
      </Text>
    </Box>
  );
}