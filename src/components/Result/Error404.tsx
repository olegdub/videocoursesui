import { Box, Heading, Text, Button } from '@chakra-ui/react';
import { Link } from 'react-router-dom';
import { sitePaths } from '../../axios';
import { colors } from '../../theme';

export default function Error404() {
  return (
    <Box textAlign="center" py={10} px={6}>
      <Heading
        display="inline-block"
        as="h2"
        size="2xl"
        bgGradient={colors.gradient}
        backgroundClip="text">
        404
      </Heading>
      <Text fontSize="18px" mt={3} mb={2}>
        Page Not Found
      </Text>
      <Text color={'gray.500'} mb={6}>
        The page you're looking for does not seem to exist
      </Text>

      <Link
          to={sitePaths.home}
      >
          <Box
              borderRadius='25px'
              p='10px 30px'
              textAlign='center'
              textTransform='capitalize'
              color='White'
              bg={colors.gradient}
              width="full"
              maxW='400px'
              margin='0 auto'
          >
              Go to Home
            </Box>
      </Link>
      
    </Box>
  );
}