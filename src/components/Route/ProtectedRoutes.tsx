import { Navigate, Outlet } from "react-router-dom"
import { useAppSelector } from "../../hooks/redux"
import { sitePaths } from "../../axios"


function ProtectedRoutes (): JSX.Element {

    const { isAuthenticated } = useAppSelector(state => state.auth)

    return isAuthenticated ? <Outlet/> : <Navigate to={sitePaths.login}/>
}

export default ProtectedRoutes