/* eslint-disable jsx-a11y/label-has-associated-control */
import { useEffect, useState } from "react";
import * as Yup from "yup";
import { Formik, Field } from "formik";
import {
  Box,
  Button,
  Checkbox,
  Flex,
  FormControl,
  FormErrorMessage,
  Heading,
  Image,
  Input,
  InputGroup,
  InputRightElement,
  Spinner,
  Stack,
  Text,
  VStack
} from "@chakra-ui/react";
import { ViewIcon, ViewOffIcon } from '@chakra-ui/icons';
import { IHandleLogin } from "../../types/auth";
import { colors } from "../../theme";
import { Link } from "react-router-dom";
import { sitePaths } from "../../axios";


interface LoginFormProps {
  handleLogin: ({ isSuccessLogin, formValue }: IHandleLogin) => void;
  resMessage: string
}


export default function LoginForm({ handleLogin, resMessage }: LoginFormProps) {

  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    setLoading(false)
  }, [resMessage])


  const [show, setShow] = useState(false)
  const handleClick = () => setShow(!show)

  const initialValues: {
    email: string;
    password: string;
  } = {
    email: "",
    password: "",
  };
  const validationSchema = Yup.object().shape({
    email: Yup.string().required("Это поле обязательно!").email('введите корректный email'),
    password: Yup.string().required("Это поле обязательно!").min(6, 'Длинна пароля должна быть минимум 6 символов!'),
  });
  const loginHandler = (formValue: { email: string; password: string }) => {
    setLoading(true);
    const loginProps: IHandleLogin = {
      isSuccessLogin: true,
      formValue
    }
    handleLogin(loginProps);
  };


  return (
    <Box>
      <Box>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={loginHandler}
        >
          {({ handleSubmit, errors, touched }) => (
            <form onSubmit={handleSubmit}>


            <Stack minH={'100vh'} direction={{ base: 'column', md: 'row' }}>
                <Flex p={8} flex={1} align={'center'} justify={'center'}>
                <Stack spacing={4} w={'full'} maxW={'md'}>
                    <Heading fontSize={'2xl'}>Sign in to your account</Heading>


                    <FormControl isInvalid={!!errors.email && touched.email}>
                    <Field
                        as={Input}
                        id="email"
                        name="email"
                        type="email"
                        variant="filled"
                        placeholder="Email"
                        minW={{ base: '200px', md: '360px' }}
                        size={{ base: 'md', md: "lg" }}
                        borderRadius='4px'
                        p={{ base: '4px', lg: '8px' }}
                    />
                    <FormErrorMessage>{errors.email}</FormErrorMessage>
                    </FormControl>


                    <FormControl isInvalid={!!errors.password && touched.password}>
                        <InputGroup size='md'>
                            <Field
                            as={Input}
                            id="password"
                            name="password"
                            type={show ? "text" : "password"}
                            variant="filled"
                            placeholder="Пароль"
                            minW={{ base: '200px', md: '360px' }}
                            size={{ base: 'md', md: "lg" }}
                            borderRadius='4px'
                            p={{ base: '4px', lg: '8px' }}
                            />
                            <InputRightElement h='100%'>
                            <Button
                                h='44px'
                                borderRadius='50%'
                                fontSize='1.5rem'
                                bg='none'
                                _focus={{ boxShadow: 'none' }}
                                onClick={handleClick}
                            >
                                {show ? <ViewOffIcon /> : <ViewIcon />}
                            </Button>
                            </InputRightElement>
                        </InputGroup>
                        <FormErrorMessage>{errors.password}</FormErrorMessage>
                        </FormControl>      

                    <Box p='10px'>
                      <Text color='red' fontSize='14px'>
                          {resMessage}
                      </Text>

                    </Box>

                    <Stack spacing={6}>
                    <Stack
                        direction={{ base: 'column', sm: 'row' }}
                        align={'start'}
                        justify={'space-between'}>
                        <Checkbox>Remember me</Checkbox>
                        <Link to={sitePaths.forgotPassword}>
                          <Text
                            textDecor='none'
                            _hover={{textDecor: 'underline'}}
                          >
                            Forgot password?
                          </Text>
                        </Link>
                    </Stack>
                    <Button
                            borderRadius='25px'
                            p='10px 30px'
                            textAlign='center'
                            textTransform='capitalize'
                            color='White'
                            bg={colors.gradient}
                            type="submit"
                            width="full"
                        >
                            { loading ? (
                                <Spinner/> 
                            ) : (
                                "Войти"
                            )}
                        </Button>


                        <Link to={sitePaths.register}>

                          <Box
                              borderRadius='25px'
                              p='10px 30px'
                              textAlign='center'
                              textTransform='capitalize'
                              color='White'
                              bg={colors.control}
                              width="full"
                              _hover={{bg: 'gray.300'}}
                          >
                              Создать новый аккаунт
                          </Box>

                        </Link>
                        
                    </Stack>
                </Stack>
                </Flex>
                <Flex flex={1}>
                <Image
                    alt={'Login Image'}
                    objectFit={'cover'}
                    src={
                    'https://images.unsplash.com/photo-1486312338219-ce68d2c6f44d?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1352&q=80'
                    }
                />
                </Flex>
            </Stack>
            </form>
          )}
        </Formik>
      </Box>
    </Box>
  );
}