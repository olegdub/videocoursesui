/* eslint-disable jsx-a11y/label-has-associated-control */
import { useEffect, useState } from "react";
import * as Yup from "yup";
import { Formik, Field } from "formik";
import {
  Box,
  Button,
  Checkbox,
  Flex,
  FormControl,
  FormErrorMessage,
  Heading,
  Image,
  Input,
  InputGroup,
  InputRightElement,
  Spinner,
  Stack,
  Text,
  VStack
} from "@chakra-ui/react";
import { ViewIcon, ViewOffIcon } from '@chakra-ui/icons';
import { colors } from "../../theme";
import { Link } from "react-router-dom";
import { sitePaths } from "../../axios";
import { IHandleRegister, IRegisterValue } from "../../types/auth";


interface RegistrationFormProps {
  handle: ({ isSuccess, formValue }: IHandleRegister) => void;
  resMessage: string
}


export default function RegistrationForm({ handle, resMessage }: RegistrationFormProps) {

  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    setLoading(false)
  }, [resMessage])


  const [show, setShow] = useState(false)
  const handleClick = () => setShow(!show)
  const [showConfirm, setShowConfirm] = useState(false)
  const handleClickConfirm = () => setShowConfirm(!showConfirm)

  const initialValues: IRegisterValue = {
    username: "",
    email: "",
    password: "",
    re_password: ""
  };
  const validationSchema = Yup.object().shape({
    username: Yup.string()
        .required("Это поле обязательно"),
    email: Yup.string()
        .required("Это поле обязательно")
        .email('введите корректный email'),
    password: Yup.string()
        .required("Это поле обязательно")
        .min(6, 'Длинна пароля должна быть минимум 6 символов'),
    re_password: Yup.string()
        // @ts-ignore
        .oneOf([Yup.ref('password'), null], 'Passwords must match')
  });
  const submitHandler = (formValue: IRegisterValue) => {
    setLoading(true);
    const formData: IHandleRegister = {
      isSuccess: true,
      formValue
    }
    handle(formData);
  };


  return (
    <Box>
      <Box>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={submitHandler}
        >
          {({ handleSubmit, errors, touched }) => (
            <form onSubmit={handleSubmit}>


            <Stack minH={'100vh'} direction={{ base: 'column', md: 'row' }}>
                <Flex p={8} flex={1} align={'center'} justify={'center'}>
                <Stack spacing={4} w={'full'} maxW={'md'}>
                    <Heading fontSize={'2xl'}>Sign up new account</Heading>


                    <FormControl isInvalid={!!errors.username && touched.username}>
                    <Field
                        as={Input}
                        id="username"
                        name="username"
                        type="username"
                        variant="filled"
                        placeholder="username"
                        minW={{ base: '200px', md: '360px' }}
                        size={{ base: 'md', md: "lg" }}
                        borderRadius='4px'
                        p={{ base: '4px', lg: '8px' }}
                    />
                    <FormErrorMessage>{errors.username}</FormErrorMessage>
                    </FormControl>


                    <FormControl isInvalid={!!errors.email && touched.email}>
                    <Field
                        as={Input}
                        id="email"
                        name="email"
                        type="email"
                        variant="filled"
                        placeholder="Email"
                        minW={{ base: '200px', md: '360px' }}
                        size={{ base: 'md', md: "lg" }}
                        borderRadius='4px'
                        p={{ base: '4px', lg: '8px' }}
                    />
                    <FormErrorMessage>{errors.email}</FormErrorMessage>
                    </FormControl>


                    <FormControl isInvalid={!!errors.password && touched.password}>
                        <InputGroup size='md'>
                            <Field
                            as={Input}
                            id="password"
                            name="password"
                            type={show ? "text" : "password"}
                            variant="filled"
                            placeholder="Пароль"
                            minW={{ base: '200px', md: '360px' }}
                            size={{ base: 'md', md: "lg" }}
                            borderRadius='4px'
                            p={{ base: '4px', lg: '8px' }}
                            />
                            <InputRightElement h='100%'>
                            <Button
                                h='44px'
                                borderRadius='50%'
                                fontSize='1.5rem'
                                bg='none'
                                _focus={{ boxShadow: 'none' }}
                                onClick={handleClick}
                            >
                                {show ? <ViewOffIcon /> : <ViewIcon />}
                            </Button>
                            </InputRightElement>
                        </InputGroup>
                        <FormErrorMessage>{errors.password}</FormErrorMessage>
                        </FormControl>    

                        <FormControl isInvalid={!!errors.re_password && touched.re_password}>
                        <InputGroup size='md'>
                            <Field
                            as={Input}
                            id="re_password"
                            name="re_password"
                            type={showConfirm ? "text" : "password"}
                            variant="filled"
                            placeholder="Повторите пароль"
                            minW={{ base: '200px', md: '360px' }}
                            size={{ base: 'md', md: "lg" }}
                            borderRadius='4px'
                            p={{ base: '4px', lg: '8px' }}
                            />
                            <InputRightElement h='100%'>
                            <Button
                                h='44px'
                                borderRadius='50%'
                                fontSize='1.5rem'
                                bg='none'
                                _focus={{ boxShadow: 'none' }}
                                onClick={handleClickConfirm}
                            >
                                {show ? <ViewOffIcon /> : <ViewIcon />}
                            </Button>
                            </InputRightElement>
                        </InputGroup>
                        <FormErrorMessage>{errors.re_password}</FormErrorMessage>
                        </FormControl>  
                      
                    <Box p='10px'>
                      <Text color='red' fontSize='14px'>
                          {resMessage}
                      </Text>

                    </Box>


                    <Stack spacing={6}>
                    <Stack
                        direction={{ base: 'column', sm: 'row' }}
                        align={'start'}
                        justify={'space-between'}>
                        <Checkbox>Remember me</Checkbox>
                        <Link to={sitePaths.login}>
                        <Text
                            textDecor='none'
                            _hover={{textDecor: 'underline'}}
                          >
                            Уже есть аккаунт?
                          </Text>
                        </Link>
                    </Stack>
                    <Button
                            borderRadius='25px'
                            p='10px 30px'
                            textAlign='center'
                            textTransform='capitalize'
                            color='White'
                            bg={colors.gradient}
                            type="submit"
                            width="full"
                        >
                            { loading ? (
                                <Spinner/> 
                            ) : (
                                "Зарегистрироваться"
                            )}
                        </Button>
                    </Stack>
                </Stack>
                </Flex>
                <Flex flex={1}>
                <Image
                    alt={'Login Image'}
                    objectFit={'cover'}
                    src={
                    'https://images.unsplash.com/photo-1486312338219-ce68d2c6f44d?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1352&q=80'
                    }
                />
                </Flex>
            </Stack>
            </form>
          )}
        </Formik>
      </Box>
    </Box>
  );
}