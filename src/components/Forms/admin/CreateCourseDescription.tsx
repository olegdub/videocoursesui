import { Button, Checkbox, CheckboxGroup, Flex, FormControl, FormHelperText, Heading, Icon, Input, Spinner, Textarea } from "@chakra-ui/react"
import { FC, PropsWithChildren, useEffect, useState } from "react"
import { FiSave } from "react-icons/fi"
import LabelWithPopover from "../../TextBlocks/LabelWithPopover"
import FileUploader from "../../FormsComponents/FileUploader"
import { useAppDispach, useAppSelector } from "../../../hooks/redux"
import { getCategories } from "../../../redux/actions/categoriesAction"
import { DescriptionVariants, descriptionPopovers } from "../../../data/createCourse"
import { useFormik } from "formik"
import { IFormProps } from "../../../types/forms"
import MDEditor from "@uiw/react-md-editor"



export interface ICourseDescriptionFormValue {
    title: string,
    description: string,
}

export type CourseDescription = {
    variant: DescriptionVariants
    initValues: ICourseDescriptionFormValue
    order: number,
}

interface ICourseDescriptionProps extends PropsWithChildren{
    form: IFormProps<ICourseDescriptionFormValue>,
    props: CourseDescription
}


const CreateCourseDescription: FC<ICourseDescriptionProps> = (p): JSX.Element => {

    const form = p.form
    const props = p.props


    const { categories, loading } = useAppSelector(state => state.categories)

    // form logic

    const [loadingForm, setLoadingForm] = useState<boolean>(false);

    useEffect( () => {
        if (!loading && loadingForm) {
            setLoadingForm(false)
        }
        
    }, [setLoadingForm, loading])

    const initialValues: ICourseDescriptionFormValue = props.initValues
    const SubmitHandler = (formValues: ICourseDescriptionFormValue) => {
        const values = {
            ...formValues
        }
        console.log(values)
        form.formHandle({
            isSuccess: true,
            formValue: values
        })
        //  save in store
        // dispatch(getCategories()) 
    }

    const formik = useFormik({
        initialValues,
        onSubmit: values => {
            setLoadingForm(true)
            SubmitHandler(values)
        },
    });


    return (         
         <Flex
         borderRadius='25px'
         bg='white'
         flexDir='column'
         w='100%'
     >
        <form onSubmit={formik.handleSubmit}>
            {/* header */}
            <Flex
                borderBottom='1px dashed'
                borderColor='gray.300'
                p='20px 40px'
                w='100%'
                alignItems='center'
                justifyContent='space-between'
            >

                <Heading
                    as='h3'
                    fontSize='24px'
                    m='10px 5px'
                >
                    Описание
                </Heading>

                <Button
                    variant='solid' 
                    colorScheme='blue'
                    borderRadius='25px'
                    p='10px 30px'
                    textAlign='center'
                    color='White'
                    bg='teal.400'
                    _hover={{bg: 'teal.200'}}
                    w='230px'
                    justifySelf='flex-end'
                    display='flex'
                    justifyContent='space-between'
                    type='submit'
                >
                    
                    <Icon as={FiSave} h='24px' w='24px'/>
                    { loadingForm ? (
                        <Spinner/>
                        ) : "Сохранить"
                    }
                </Button>

            </Flex>

            {/* row */}
            <Flex
                p='20px 40px'
                w='100%'
            >
                <LabelWithPopover props={descriptionPopovers[0]}/>

                <Flex
                    maxW='75%'
                    flex='1 0 75%'
                    pl='20px'
                    flexDir='column'
                    alignItems='flex-start'
                >
                    <FormControl>
                        
                        <Input
                            id="title"
                            name='title'
                            onChange={formik.handleChange}
                            value={formik.values.title}
                            type="text" 
                            focusBorderColor='cyan.400'
                        />
                        <FormHelperText>We'll never share your email.</FormHelperText>
                    </FormControl>
                </Flex>

            </Flex>

            {/* row */}
            <Flex
                p='20px 40px'
                w='100%'
            >
                <LabelWithPopover props={descriptionPopovers[1]}/>

                <Flex
                    maxW='75%'
                    flex='1 0 75%'
                    pl='20px'
                    flexDir='column'
                    alignItems='flex-start'
                >
                    <FormControl>
                        
                        {/* <Textarea 
                            placeholder='Here is a sample placeholder' 
                            name='description'
                            onChange={formik.handleChange}
                            value={formik.values.description}    
                        /> */}
                        <MDEditor
                            id='description'
                            height={200} 
                            value={formik.values.description} 
                            onChange={(value) => formik.setFieldValue('description', value)} 
                        />
                        <FormHelperText>We'll never share your email.</FormHelperText>
                    </FormControl>
                </Flex>

            </Flex>

         </form>

         {p.children}

        </Flex>

    )
}

export default CreateCourseDescription