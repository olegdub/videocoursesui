import { Button, Checkbox, CheckboxGroup, Flex, FormControl, FormHelperText, Heading, Icon, Input, Spinner, Textarea } from "@chakra-ui/react"
import { FC, useEffect, useState } from "react"
import { FiSave } from "react-icons/fi"
import LabelWithPopover from "../../TextBlocks/LabelWithPopover"
import FileUploader from "../../FormsComponents/FileUploader"
import { useAppDispach, useAppSelector } from "../../../hooks/redux"
import { getCategories } from "../../../redux/actions/categoriesAction"
import { authorPopovers } from "../../../data/createCourse"
import { useFormik } from "formik"
import { IFormProps } from "../../../types/forms"
import MDEditor from "@uiw/react-md-editor"





export type CourseAuthorFormValue = {
    fullName: string,
    workExperience: string,
    description: string
}


const CreateCourseAboutAuthor: FC<IFormProps<CourseAuthorFormValue>> = ({formHandle, resMessage}): JSX.Element => {


    const [ files, setFiles ] = useState<File[]>([])

    const saveFiles = (newFiles: any[]) => {
        setFiles([...files].concat(newFiles))
    }

    const dispatch = useAppDispach()

    const { categories, loading } = useAppSelector(state => state.categories)

    useEffect(() => {
        if(categories.length === 0 && !loading) {
            dispatch(getCategories()) 
        }   
    }, [categories, loading, dispatch])


    // form logic

    const [loadingForm, setLoadingForm] = useState<boolean>(false);

    useEffect( () => {
        if (!loading && loadingForm) {
            setLoadingForm(false)
        }
        
    }, [setLoadingForm, loading])

    const initialValues: CourseAuthorFormValue = {
        fullName: '',
        workExperience: '',
        description: ''
    }
    const SubmitHandler = (formValues: CourseAuthorFormValue) => {
        console.log(formValues)
        formHandle({
            isSuccess: true,
            formValue: formValues
        })
        //  save in store
        // dispatch(getCategories()) 
    }

    const formik = useFormik({
        initialValues,
        onSubmit: values => {
            setLoadingForm(true)
            SubmitHandler(values)
        },
    });

    return (         
         <Flex
         borderRadius='25px'
         bg='white'
         flexDir='column'
         w='100%'
     >
        <form onSubmit={formik.handleSubmit}>
            {/* header */}
            <Flex
                borderBottom='1px dashed'
                borderColor='gray.300'
                p='20px 40px'
                w='100%'
                alignItems='center'
                justifyContent='space-between'
            >

                <Heading
                    as='h3'
                    fontSize='24px'
                    m='10px 5px'
                >
                    Автор
                </Heading>

                <Button
                    variant='solid' 
                    colorScheme='blue'
                    borderRadius='25px'
                    p='10px 30px'
                    textAlign='center'
                    color='White'
                    bg='teal.400'
                    _hover={{bg: 'teal.200'}}
                    w='230px'
                    justifySelf='flex-end'
                    display='flex'
                    justifyContent='space-between'
                    type='submit'
                >
                    
                    <Icon as={FiSave} h='24px' w='24px'/>
                    { loadingForm ? (
                        <Spinner/>
                        ) : "Сохранить"
                    }
                </Button>

            </Flex>

            {/* row */}
            <Flex
                p='20px 40px'
                w='100%'
            >
                <LabelWithPopover props={authorPopovers[0]}/>

                <Flex
                    maxW='75%'
                    flex='1 0 75%'
                    pl='20px'
                    flexDir='column'
                    alignItems='flex-start'
                >
                    <FormControl>
                        
                        <Input
                            id="fullName"
                            name='fullName'
                            onChange={formik.handleChange}
                            value={formik.values.fullName}
                            type="text" 
                            focusBorderColor='cyan.400'
                        />
                        <FormHelperText>We'll never share your email.</FormHelperText>
                    </FormControl>
                </Flex>

            </Flex>

            {/* row */}
            <Flex
                p='20px 40px'
                w='100%'
            >
                <LabelWithPopover props={authorPopovers[1]}/>

                <Flex
                    maxW='75%'
                    flex='1 0 75%'
                    pl='20px'
                    flexDir='column'
                    alignItems='flex-start'
                >
                    <FormControl>
                        
                        <MDEditor
                            id='description'
                            height={200} 
                            value={formik.values.description} 
                            onChange={(value) => formik.setFieldValue('description', value)} 
                        />
                        <FormHelperText>We'll never share your email.</FormHelperText>
                    </FormControl>
                </Flex>

            </Flex>

            {/* row */}
            <Flex
                p='20px 40px'
                w='100%'
            >
                <LabelWithPopover props={authorPopovers[2]}/>

                <Flex
                    maxW='75%'
                    flex='1 0 75%'
                    pl='20px'
                    flexDir='column'
                    alignItems='flex-start'
                >
                    <FormControl>
                        
                        <Input 
                            name='workExperience'
                            onChange={formik.handleChange}
                            value={formik.values.workExperience}
                            type='text'
                            
                        />
                        <FormHelperText>We'll never share your email.</FormHelperText>
                    </FormControl>
                </Flex>

            </Flex>

         </form>

        </Flex>

    )
}

export default CreateCourseAboutAuthor