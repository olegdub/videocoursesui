import { Button, Checkbox, CheckboxGroup, Flex, FormControl, FormHelperText, Heading, Icon, Input, Spinner, Textarea } from "@chakra-ui/react"
import { FC, useEffect, useState } from "react"
import { FiSave } from "react-icons/fi"
import LabelWithPopover from "../../TextBlocks/LabelWithPopover"
import FileUploader from "../../FormsComponents/FileUploader"
import { useAppDispach, useAppSelector } from "../../../hooks/redux"
import { getCategories } from "../../../redux/actions/categoriesAction"
import { basePopovers } from "../../../data/createCourse"
import { useFormik } from "formik"
import { IFormProps } from "../../../types/forms"
import { CopyIcon, DeleteIcon } from "@chakra-ui/icons"





export type CourseBaseFormValue = {
    title: string,
    description: string,
    preview: File | null, // (image)
    intro: File | null, //  (video)
    courseVideoDuration: number,
    categories: string[]
}

interface IFilesUpload {
    id: string,
    file: File       
}


const CreateCourseBase: FC<IFormProps<CourseBaseFormValue>> = ({formHandle, resMessage}): JSX.Element => {


    

    const [ previewFile, setPreviewFile ] = useState<IFilesUpload[]>([])
    const [ introFile, setIntroFile ] = useState<IFilesUpload[]>([])

    const savePreviewFile = (newFiles: any[]) => {
        setPreviewFile([...previewFile].concat(newFiles))
    }
    const saveIntroFile = (newFiles: any[]) => {
        setIntroFile([...introFile].concat(newFiles))
    }

    const deleteFile = (fileId: string, ifPreview: boolean) => {
        if (ifPreview){
            setPreviewFile(previewFile.filter(f => f.id != fileId))
        }
        setIntroFile(introFile.filter(f => f.id != fileId))
    }
    

    const dispatch = useAppDispach()

    const { categories, loading } = useAppSelector(state => state.categories)

    useEffect(() => {
        if(categories.length === 0 && !loading) {
            dispatch(getCategories()) 
        }   
    }, [categories, loading, dispatch])


    // form logic

    const [loadingForm, setLoadingForm] = useState<boolean>(false);

    useEffect( () => {
        if (!loading && loadingForm) {
            setLoadingForm(false)
        }
        
    }, [setLoadingForm, loading])

    const initialValues: CourseBaseFormValue = {
        title: '',
        description: '',
        preview: null, // (image)
        intro: null, //  (video)
        courseVideoDuration: 100,
        categories: []
    }
    const SubmitHandler = (formValues: CourseBaseFormValue) => {
        const values = {
            ...formValues,
            preview: introFile[0].file,
            intro: introFile[0].file
        }
        console.log(values)
        formHandle({
            isSuccess: true,
            formValue: values
        })
        //  save in store
        // dispatch(getCategories()) 
    }

    const formik = useFormik({
        initialValues,
        onSubmit: values => {
            setLoadingForm(true)
            SubmitHandler(values)
        },
    });

    const Checkboxes = categories.map( el => (
        <Checkbox key={el.id} value={el.categoryName} size='md' name='categories' onChange={formik.handleChange}>
            {el.categoryName}
        </Checkbox>
    ))


    return (         
         <Flex
         borderRadius='25px'
         bg='white'
         flexDir='column'
         w='100%'
     >
        <form onSubmit={formik.handleSubmit}>
            {/* header */}
            <Flex
                borderBottom='1px dashed'
                borderColor='gray.300'
                p='20px 40px'
                w='100%'
                alignItems='center'
                justifyContent='space-between'
            >

                <Heading
                    as='h3'
                    fontSize='24px'
                    m='10px 5px'
                >
                    Base
                </Heading>

                <Button
                    variant='solid' 
                    colorScheme='blue'
                    borderRadius='25px'
                    p='10px 30px'
                    textAlign='center'
                    color='White'
                    bg='teal.400'
                    _hover={{bg: 'teal.200'}}
                    w='230px'
                    justifySelf='flex-end'
                    display='flex'
                    justifyContent='space-between'
                    type='submit'
                >
                    
                    <Icon as={FiSave} h='24px' w='24px'/>
                    { loadingForm ? (
                        <Spinner/>
                        ) : "Сохранить"
                    }
                </Button>

            </Flex>

            {/* row */}
            <Flex
                p='20px 40px'
                w='100%'
            >
                <LabelWithPopover props={basePopovers[0]}/>

                <Flex
                    maxW='75%'
                    flex='1 0 75%'
                    pl='20px'
                    flexDir='column'
                    alignItems='flex-start'
                >
                    <FormControl>
                        
                        <Input
                            id="title"
                            name='title'
                            onChange={formik.handleChange}
                            value={formik.values.title}
                            type="text" 
                            focusBorderColor='cyan.400'
                        />
                        <FormHelperText>We'll never share your email.</FormHelperText>
                    </FormControl>
                </Flex>

            </Flex>

            {/* row */}
            <Flex
                p='20px 40px'
                w='100%'
            >
                <LabelWithPopover props={basePopovers[1]}/>

                <Flex
                    maxW='75%'
                    flex='1 0 75%'
                    pl='20px'
                    flexDir='column'
                    alignItems='flex-start'
                >
                    <FormControl>
                        
                        <CheckboxGroup
                            colorScheme='cyan' 
                            defaultValue={[]}>
                            <Flex
                                flexWrap='wrap'
                                gap='15px'
                            >
                                {Checkboxes}
                            </Flex>
                        </CheckboxGroup>
                        <FormHelperText>We'll never share your email.</FormHelperText>
                    </FormControl>
                </Flex>

            </Flex>

            {/* row */}
            {/* <Flex
                p='20px 40px'
                w='100%'
            >
                <LabelWithPopover props={basePopovers[2]}/>

                <Flex
                    maxW='75%'
                    flex='1 0 75%'
                    pl='20px'
                    flexDir='column'
                    alignItems='flex-start'
                >
                    <FormControl>
                        
                        <FileUploader setFiles={savePreviewFile}/>
                        <Flex
                            flexDir='column'
                            gap={'10px'}
                            mt='10px'
                        >
                            {previewFile.map( f => (
                                <Flex
                                    bg='red.300'
                                    justifyContent='space-between'
                                    p='15px 25px'
                                    color='white'
                                    key={f.id}
                                >
                                    <CopyIcon/>
                                    {f.file.name}
                                    <DeleteIcon onClick={()=> {deleteFile(f.id, true)}}/>
                                </Flex>
                            ))}
                        </Flex>
                        <FormHelperText>We'll never share your email.</FormHelperText>
                    </FormControl>
                </Flex>

            </Flex> */}

            {/* row */}
            <Flex
                p='20px 40px'
                w='100%'
            >
                <LabelWithPopover props={basePopovers[3]}/>

                <Flex
                    maxW='75%'
                    flex='1 0 75%'
                    pl='20px'
                    flexDir='column'
                    alignItems='flex-start'
                >
                    <FormControl>
                        
                    <FileUploader setFiles={saveIntroFile}/>
                        <Flex
                            flexDir='column'
                            gap={'10px'}
                            mt='10px'
                        >
                            {introFile.map( f => (
                                <Flex
                                    bg='red.300'
                                    justifyContent='space-between'
                                    p='15px 25px'
                                    color='white'
                                    key={f.id}
                                >
                                    <CopyIcon/>
                                    {f.file.name}
                                    <DeleteIcon onClick={()=> {deleteFile(f.id, true)}}/>
                                </Flex>
                            ))}
                        </Flex>
                        <FormHelperText>We'll never share your email.</FormHelperText>
                    </FormControl>
                </Flex>

            </Flex>

            {/* row */}
            <Flex
                p='20px 40px'
                w='100%'
            >
                <LabelWithPopover props={basePopovers[4]}/>

                <Flex
                    maxW='75%'
                    flex='1 0 75%'
                    pl='20px'
                    flexDir='column'
                    alignItems='flex-start'
                >
                    <FormControl>
                        
                        <Textarea 
                            placeholder='Here is a sample placeholder' 
                            name='description'
                            onChange={formik.handleChange}
                            value={formik.values.description}    
                        />
                        <FormHelperText>We'll never share your email.</FormHelperText>
                    </FormControl>
                </Flex>

            </Flex>

            {/* row */}
            <Flex
                p='20px 40px'
                w='100%'
            >
                <LabelWithPopover props={basePopovers[5]}/>

                <Flex
                    maxW='75%'
                    flex='1 0 75%'
                    pl='20px'
                    flexDir='column'
                    alignItems='flex-start'
                >
                    <FormControl>
                        
                        <Input 
                            name='courseVideoDuration'
                            onChange={formik.handleChange}
                            value={formik.values.courseVideoDuration}
                            type='number'
                            
                        />
                        <FormHelperText>We'll never share your email.</FormHelperText>
                    </FormControl>
                </Flex>

            </Flex>

         </form>

        </Flex>

    )
}

export default CreateCourseBase