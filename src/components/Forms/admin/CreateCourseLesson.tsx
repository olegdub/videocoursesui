import { Box, Button, Checkbox, CheckboxGroup, Flex, FormControl, FormHelperText, Heading, Icon, Input, Spinner, Textarea } from "@chakra-ui/react"
import { FC, PropsWithChildren, useEffect, useState } from "react"
import { FiSave } from "react-icons/fi"
import LabelWithPopover from "../../TextBlocks/LabelWithPopover"
import FileUploader from "../../FormsComponents/FileUploader"
import { useAppDispach, useAppSelector } from "../../../hooks/redux"
import { getCategories } from "../../../redux/actions/categoriesAction"
import { DescriptionVariants, LessonVariants, lessonPopovers } from "../../../data/createCourse"
import { useFormik } from "formik"
import { IFormProps } from "../../../types/forms"
import { CopyIcon, DeleteIcon } from "@chakra-ui/icons"
import MDEditor from "@uiw/react-md-editor"




export interface ICourseLessonFormValue {
    title: string,
    description: string,
    video: File | null,
    textContent: string
}

export type CourseLesson = {
    initValues: ICourseLessonFormValue
    order: number,
    variant: LessonVariants,
    lessonsCount: number,
    parentModule: number
}

interface ICourseLessonProps extends PropsWithChildren{
    form: IFormProps<ICourseLessonFormValue>,
    props: CourseLesson
}

interface IFilesUpload {
    id: string,
    file: File       
}

const CreateCourseLesson: FC<ICourseLessonProps> = (p): JSX.Element => {

    const form = p.form
    const props = p.props

    const [ files, setFiles ] = useState<IFilesUpload[]>([])

    const saveFiles = (newFiles: any[]) => {
        setFiles([...files].concat(newFiles))
    }
    const deleteFile = (fileId: string, ifPreview: boolean) => {
        setFiles(files.filter(f => f.id != fileId))
    }


    const { categories, loading } = useAppSelector(state => state.categories)

    // form logic

    const [loadingForm, setLoadingForm] = useState<boolean>(false);

    useEffect( () => {
        if (!loading && loadingForm) {
            setLoadingForm(false)
        }
        
    }, [setLoadingForm, loading])

    const initialValues: ICourseLessonFormValue = props.initValues
    const SubmitHandler = (formValues: ICourseLessonFormValue) => {
        const values = {
            ...formValues,
            video: files[0].file
        }
        console.log(values)
        form.formHandle({
            isSuccess: true,
            formValue: values
        })
        //  save in store
        // dispatch(getCategories()) 
    }

    const formik = useFormik({
        initialValues,
        onSubmit: values => {
            setLoadingForm(true)
            SubmitHandler(values)
        }
    });


    return (         
         <Flex
         borderRadius='25px'
         bg='white'
         flexDir='column'
         w='100%'
         h='100%'
     >
        <form onSubmit={formik.handleSubmit}>
            {/* header */}
            <Flex
                borderTopRadius='25px'
                bg='white'
                borderBottom='1px dashed'
                borderColor='gray.300'
                p='20px 40px'
                w='100%'
                alignItems='center'
                justifyContent='space-between'
            >

                <Heading
                    as='h3'
                    fontSize='24px'
                    m='10px 5px'
                >
                    Урок №{`${props.parentModule + 1}.${props.order + 1}`}
                </Heading>

                <Button
                    variant='solid' 
                    colorScheme='blue'
                    borderRadius='25px'
                    p='10px 30px'
                    textAlign='center'
                    color='White'
                    bg='teal.400'
                    _hover={{bg: 'teal.200'}}
                    w='230px'
                    justifySelf='flex-end'
                    display='flex'
                    justifyContent='space-between'
                    type='submit'
                >
                    
                    <Icon as={FiSave} h='24px' w='24px'/>
                    { loadingForm ? (
                        <Spinner/>
                        ) : "Сохранить"
                    }
                </Button>

            </Flex>

            {/* row */}
            <Flex
                p='20px 40px'
                w='100%'
            >
                <LabelWithPopover props={lessonPopovers[0]}/>

                <Flex
                    maxW='75%'
                    flex='1 0 75%'
                    pl='20px'
                    flexDir='column'
                    alignItems='flex-start'
                >
                    <FormControl>
                        
                        <Input
                            id="title"
                            name='title'
                            onChange={formik.handleChange}
                            value={formik.values.title}
                            type="text" 
                            focusBorderColor='cyan.400'
                            bg='white'
                        />
                        <FormHelperText>We'll never share your email.</FormHelperText>
                    </FormControl>
                </Flex>

            </Flex>

            {/* row */}
            <Flex
                p='20px 40px'
                w='100%'
            >
                <LabelWithPopover props={lessonPopovers[1]}/>

                <Flex
                    maxW='75%'
                    flex='1 0 75%'
                    pl='20px'
                    flexDir='column'
                    alignItems='flex-start'
                >
                    <FormControl>
                        
                        <Textarea 
                            placeholder='Here is a sample placeholder' 
                            name='description'
                            bg='white'
                            onChange={formik.handleChange}
                            value={formik.values.description}    
                        />
                        <FormHelperText>We'll never share your email.</FormHelperText>
                    </FormControl>
                </Flex>
            </Flex>

                     {/* row */}
            <Flex
                p='20px 40px'
                w='100%'
            >
                <LabelWithPopover props={lessonPopovers[2]}/>

                <Flex
                    maxW='75%'
                    flex='1 0 75%'
                    pl='20px'
                    flexDir='column'
                    alignItems='flex-start'
                >
                    <FormControl>
                        
                    <FileUploader setFiles={saveFiles}/>
                        <Flex
                            flexDir='column'
                            gap={'10px'}
                            mt='10px'
                        >
                            {files.map( f => (
                                <Flex
                                    bg='red.300'
                                    justifyContent='space-between'
                                    p='15px 25px'
                                    color='white'
                                    key={f.id}
                                >
                                    <CopyIcon/>
                                    {f.file.name}
                                    <DeleteIcon onClick={()=> {deleteFile(f.id, true)}}/>
                                </Flex>
                            ))}
                        </Flex>
                        <FormHelperText>We'll never share your email.</FormHelperText>
                    </FormControl>
                </Flex>

            </Flex>

                {/* row */}
            <Flex
                p='20px 40px'
                w='100%'
            >
                <LabelWithPopover props={lessonPopovers[3]}/>

                <Flex
                    maxW='75%'
                    flex='1 0 75%'
                    pl='20px'
                    flexDir='column'
                    alignItems='flex-start'
                >
                    <FormControl>
                        
                        <MDEditor
                            id='description'
                            height={200} 
                            value={formik.values.textContent} 
                            onChange={(value) => formik.setFieldValue('textContent', value)} 
                        />
                        <FormHelperText>We'll never share your email.</FormHelperText>
                    </FormControl>
                </Flex>

            </Flex>

                <Flex
                    justifyContent='flex-end'
                    w='100%'
                >
                    {p.children}
                </Flex>

         </form>

         

        </Flex>

    )
}

export default CreateCourseLesson