import { Button, Checkbox, CheckboxGroup, Flex, FormControl, FormHelperText, Heading, Icon, Input, Spinner, Textarea } from "@chakra-ui/react"
import { FC, PropsWithChildren, useEffect, useState } from "react"
import { FiSave } from "react-icons/fi"
import LabelWithPopover from "../../TextBlocks/LabelWithPopover"
import FileUploader from "../../FormsComponents/FileUploader"
import { useAppDispach, useAppSelector } from "../../../hooks/redux"
import { getCategories } from "../../../redux/actions/categoriesAction"
import { DescriptionVariants, modulePopovers } from "../../../data/createCourse"
import { useFormik } from "formik"
import { IFormProps } from "../../../types/forms"
import { IModuleLesson } from "../../../types/admin"
import { AddIcon, DeleteIcon } from "@chakra-ui/icons"


// export interface ILessonVideo {
//     id: number,
//     title: string,
//     description: string,
//     order: number,
//     variant: string,
//     video: File
// }


export interface ICourseModuleFormValue {
    title: string,
    description: string,
}

export type CourseModule = {
    initValues: ICourseModuleFormValue
    order: number,
    lessons: IModuleLesson[],
    removeModuleHandler: () => void
    addLessonHandler: () => void,
    modulesCount: number
}

interface ICourseModuleProps extends PropsWithChildren{
    form: IFormProps<ICourseModuleFormValue>,
    props: CourseModule
}


const CreateCourseModule: FC<ICourseModuleProps> = (p): JSX.Element => {

    const form = p.form
    const props = p.props


    const { categories, loading } = useAppSelector(state => state.categories)

    // form logic

    const [loadingForm, setLoadingForm] = useState<boolean>(false);

    useEffect( () => {
        if (!loading && loadingForm) {
            setLoadingForm(false)
        }
        
    }, [setLoadingForm, loading])

    const initialValues: ICourseModuleFormValue = props.initValues
    const SubmitHandler = (formValues: ICourseModuleFormValue) => {
        const values = {
            ...formValues
        }
        console.log(values)
        form.formHandle({
            isSuccess: true,
            formValue: values
        })
        //  save in store
        // dispatch(getCategories()) 
    }

    const formik = useFormik({
        initialValues,
        onSubmit: values => {
            setLoadingForm(true)
            SubmitHandler(values)
        },
    });

    return (         
         <Flex
         borderRadius='25px'
         bg='gray.100'
         flexDir='column'
         w='100%'
     >
        <form onSubmit={formik.handleSubmit}>
            {/* header */}
            <Flex
                borderTopRadius='25px'
                bg='white'
                borderBottom='1px dashed'
                borderColor='gray.300'
                p='20px 40px'
                w='100%'
                alignItems='center'
                justifyContent='space-between'
            >

                <Heading
                    as='h3'
                    fontSize='24px'
                    m='10px 5px'
                >
                    Модуль №{`${props.order + 1} из ${props.modulesCount}`}
                </Heading>

                <Button
                    variant='solid' 
                    colorScheme='blue'
                    borderRadius='25px'
                    p='10px 30px'
                    textAlign='center'
                    color='White'
                    bg='teal.400'
                    _hover={{bg: 'teal.200'}}
                    w='230px'
                    justifySelf='flex-end'
                    display='flex'
                    justifyContent='space-between'
                    type='submit'
                >
                    
                    <Icon as={FiSave} h='24px' w='24px'/>
                    { loadingForm ? (
                        <Spinner/>
                        ) : "Сохранить"
                    }
                </Button>

            </Flex>

            {/* row */}
            <Flex
                p='20px 40px'
                w='100%'
            >
                <LabelWithPopover props={modulePopovers[0]}/>

                <Flex
                    maxW='75%'
                    flex='1 0 75%'
                    pl='20px'
                    flexDir='column'
                    alignItems='flex-start'
                >
                    <FormControl>
                        
                        <Input
                            id="title"
                            name='title'
                            onChange={formik.handleChange}
                            value={formik.values.title}
                            type="text" 
                            focusBorderColor='cyan.400'
                            bg='white'
                        />
                        <FormHelperText>We'll never share your email.</FormHelperText>
                    </FormControl>
                </Flex>

            </Flex>

            {/* row */}
            <Flex
                p='20px 40px'
                w='100%'
            >
                <LabelWithPopover props={modulePopovers[1]}/>

                <Flex
                    maxW='75%'
                    flex='1 0 75%'
                    pl='20px'
                    flexDir='column'
                    alignItems='flex-start'
                >
                    <FormControl>
                        
                        <Textarea 
                            placeholder='Here is a sample placeholder' 
                            name='description'
                            bg='white'
                            onChange={formik.handleChange}
                            value={formik.values.description}    
                        />
                        <FormHelperText>We'll never share your email.</FormHelperText>
                    </FormControl>
                </Flex>

            </Flex>

         </form>

         <Flex
            flexDir='column'
            w='calc(100% - 80px)'
            m='0 auto'
            borderRadius='25px'
            mb='20pz'
        >
            {p.children}
        </Flex>

        <Flex
            justifyContent='center'
            mt='20px'
        >
            <Button
                colorScheme='teal'
                variant='outline'
                borderRadius='25px'
                border='2px dashed'
                borderColor='teal'
                p='10px 30px'
                textAlign='center'
                color='teal'
                // bg='teal.400'
                // _hover={{bg: 'teal.200'}}
                w='230px'
                justifySelf='flex-end'
                display='flex'
                justifyContent='space-between'
                onClick={props.addLessonHandler}
            >
                <AddIcon/>
                Добавить урок
            </Button>
        </Flex>

        <Flex
            justifyContent='flex-end'
        >

            <Button
                variant='solid' 
                colorScheme='blue'
                borderRadius='25px'
                m='20px 40px 40px 0'
                p='10px 30px'
                textAlign='center'
                color='White'
                bg='red.400'
                _hover={{bg: 'red.200'}}
                w='230px'
                alignSelf='flex-end'
                display='flex'
                justifyContent='space-between'
                onClick={()=> {props.removeModuleHandler()}}
            >
                <DeleteIcon w='18px' h='18px'/>Удалить модуль
            </Button>
        </Flex>


        </Flex>

    )
}

export default CreateCourseModule