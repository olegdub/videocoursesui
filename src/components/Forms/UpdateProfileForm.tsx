/* eslint-disable jsx-a11y/label-has-associated-control */
import { useState } from "react";
import * as Yup from "yup";
import { Formik, Field } from "formik";
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormErrorMessage,
  Input,
  InputGroup,
  Spinner,
  Stack,
  Text,
} from "@chakra-ui/react";
import { colors } from "../../theme";
import { IHandleUpdateUser, IUpdateUser, IUpdateUserInitial } from "../../types/user";


interface FormProps {
  handle: ({ isSuccessUpdate, formValue }: IHandleUpdateUser) => void;
  resMessage: string,
  initialState: IUpdateUserInitial
}


export default function UpdateProfileForm({ handle, initialState, resMessage }: FormProps) {

  const [loading, setLoading] = useState<boolean>(false);

  const initialValues: IUpdateUser = initialState;
  const validationSchema = Yup.object().shape({
    name: Yup.string().required("Это поле обязательно!"),
    surname: Yup.string().required("Это поле обязательно!"),
  });
  const submitHandler = (formValue: IUpdateUser) => {
    setLoading(true);
    const loginProps: IHandleUpdateUser = {
      isSuccessUpdate: true,
      formValue
    }
    handle(loginProps);
  };


  return (
    <Box>
      <Box>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={submitHandler}
        >
          {({ handleSubmit, errors, touched, dirty }) => (
            <form onSubmit={handleSubmit}>
                <Flex p={8} flex={1} align={'center'} justify={'center'}>
                <Stack spacing={8} w={'full'} maxW={'full'}>

                    <Flex
                        alignItems='center'
                        w='100%'
                    >
                        <Text
                            whiteSpace='nowrap'
                            fontWeight='bold'
                            mr='2rem'
                            w='200px'
                            fontSize='16px'
                        >
                            Ваше имя: 
                        </Text>

                        <FormControl isInvalid={!!errors.name && touched.name}>
                        <Field
                            as={Input}
                            id="name"
                            name="name"
                            type="text"
                            variant="filled"
                            placeholder="Имя"
                            minW={{ base: '200px', md: '360px' }}
                            size={{ base: 'md', md: "lg" }}
                            w='full'
                            borderRadius='4px'
                            p={{ base: '4px', lg: '8px' }}
                        />
                        </FormControl>
                    </Flex>

                    
                    <Flex
                        alignItems='center'
                        w='100%'
                    >
                        <Text
                            fontWeight='bold'
                            mr='2rem'
                            w='200px'
                            fontSize='16px'
                        >
                            Ваша фамилия: 
                        </Text>

                        <FormControl isInvalid={!!errors.surname && touched.surname}>
                            <InputGroup size='md'>
                                <Field
                                as={Input}
                                id="surname"
                                name="surname"
                                type={'text'}
                                variant="filled"
                                placeholder="Фамилия"
                                minW={{ base: '200px', md: '360px' }}
                                size={{ base: 'md', md: "lg" }}
                                borderRadius='4px'
                                p={{ base: '4px', lg: '8px' }}
                                />
                            </InputGroup>
                            </FormControl>      
                    </Flex>

                    <Flex
                        alignItems='center'
                        w='100%'
                    >
                        <Text
                            whiteSpace='nowrap'
                            fontWeight='bold'
                            mr='2rem'
                            w='200px'
                            fontSize='16px'
                        >
                            Ваш email: 
                        </Text>

                        {/* <FormControl isInvalid={!!errors.name && touched.name}> */}
                        <Field
                            as={Input}
                            id="email"
                            name="email"
                            type="email"
                            variant="filled"
                            placeholder="Email"
                            minW={{ base: '200px', md: '360px' }}
                            size={{ base: 'md', md: "lg" }}
                            w='full'
                            borderRadius='4px'
                            p={{ base: '4px', lg: '8px' }}
                        />
                        {/* </FormControl> */}
                    </Flex>

                    <Box p='10px'>
                      <Text color='red' fontSize='14px'>
                          {resMessage}
                      </Text>
                    </Box>

                    <Stack spacing={6}>
                    <Button
                            borderRadius='25px'
                            p='10px 30px'
                            textAlign='center'
                            textTransform='capitalize'
                            color='White'
                            bg={colors.gradient}
                            type="submit"
                            width="full"
                            disabled={!dirty}
                        >
                            { loading ? (
                                <Spinner/> 
                            ) : (
                                "Обновить"
                            )}
                        </Button>
                    </Stack>
                </Stack>
                </Flex>
            </form>
          )}
        </Formik>
      </Box>
    </Box>
  );
}