import { Button, Card, CardBody, CardFooter, Divider, Flex, Heading, Image, Stack, Text } from '@chakra-ui/react'
import { FunctionComponent } from 'react'
import { useNavigate } from 'react-router-dom'
import { CourseCardProps } from './interfaces'


const BigCard: FunctionComponent<CourseCardProps> = ({course}) =>{
    
    const navigate = useNavigate()

    const onGoToCourseClick = () => {
        navigate(`/course/${course.id}`)
    }

    return (
        <Card
            direction={{ base: 'column', sm: 'row' }}
            overflow='hidden'
            variant='outline'
            borderRadius='25px'
            minH='300px'
        >
            <Image
                objectFit='cover'
                maxW={{ base: '100%', sm: '300px', md: '500px' }}
                src={course.image}
                alt='Caffe Latte'
            />

            <Stack
                w='100%'
            >
                <CardBody>
                <Heading size='md'>{course.title}</Heading>

                <Text py='2'>
                    {course.author}
                </Text>
                </CardBody>

                <Divider borderColor='gray' borderStyle='dashed'/>

                <CardFooter>

                <Flex
                    justifyContent='space-between'
                    w='100%'
                >
                    <Flex
                        alignItems='center'
                        mr='10px'
                    >
                        <Text
                            fontSize='14px'
                            color='gray'
                            textDecor='line-through'
                            p='5px'
                        >
                            {course.oldPrice} ₽
                        </Text>

                        <Text
                            fontSize='18px'
                            fontWeight='bold'
                            color='#000'
                            
                            p='5px'
                        >
                            {course.currentPrice} ₽
                        </Text>
                    </Flex>
                    <Button 
                        variant='solid' 
                        colorScheme='blue'
                        borderRadius='25px'
                        p='10px 30px'
                        textAlign='center'
                        color='White'
                        bg='linear-gradient(94.23deg, #FDF59E -4.24%, #EE9EA4 50.22%, #9389BE 105.83%);'
                        _hover={{bg: 'gray.300'}}
                        onClick={onGoToCourseClick}
                    >
                        Попробовать курс
                    </Button>
                </Flex>

                
                </CardFooter>
            </Stack>
            </Card>
    )
}

export default BigCard