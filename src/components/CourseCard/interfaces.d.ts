export interface CourseCardProps {
    course: ICourseCard
}

export interface ICourseCard {
    id: number,
    image: string,
    title: string,
    author: string,
    oldPrice: string,
    currentPrice: string,
    link: string
}