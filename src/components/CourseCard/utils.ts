import { faker } from "@faker-js/faker";
import { ICourseCard } from "./interfaces";
import { sitePaths } from "../../axios";
import { removeLastDirectoryPartOf } from "../../utils";



const getCardsData = (count: number, isAuth=false): ICourseCard[] => {   

    const cardsData: ICourseCard[] = []

    const courseLink = isAuth ? 
        removeLastDirectoryPartOf(sitePaths.courseView) : 
        removeLastDirectoryPartOf(sitePaths.courseBrand)

    for (let i = 0; i < count; i++) {
        cardsData.push(
            {
                id: i+1,
                image: faker.image.animals(340, 200),
                title: faker.lorem.lines(1),
                author: faker.name.fullName(),
                oldPrice: faker.commerce.price(4000, 5000, 0),
                currentPrice: faker.commerce.price(2000, 4000, 0),
                link: `${courseLink}/${i+1}`
            }
        );
    }

    return cardsData
}

export default getCardsData

