import { Box, Button, Text } from "@chakra-ui/react"
import { FC } from "react"
import { Link } from "react-router-dom"


export interface ISimpleMenuItem {
    id: number,
    text: string,
    link: string,
    handleClick: (currentMenuIteIid: number) => void
}

const SimpleMenuItem: FC<ISimpleMenuItem> = (props): JSX.Element => {

    return (
        <Box
                whiteSpace='nowrap'
                p='10px 20px'
                textTransform='capitalize'
                color={'black'}
                bg='none'
                textDecor='underline'
                cursor='pointer'
                onClick={() => {props.handleClick(props.id)}}
                _active={{bg:'inherit'}}
                _focus={{bg:'inherit'}}
                _hover={{bg: 'inherit', color: 'blue.300', textDecor:'none'}}

            >
                <Link to={props.link}>
                
                    <Text
                        fontSize='16px'
                    >
                        {props.text}
                    </Text>

                </Link>

            </Box>
    )
}

export default SimpleMenuItem