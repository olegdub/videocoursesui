import { FC, PropsWithChildren } from "react"
import { Popover, PopoverArrow, PopoverBody, PopoverCloseButton, PopoverContent, PopoverHeader, PopoverTrigger } from '@chakra-ui/react'

export interface IMyPopover {
    headerText: string,
    bodyText: string
}

export interface IMyPopoverProps extends PropsWithChildren{
    data: IMyPopover
}

const MyPopover: FC<IMyPopoverProps> = (props) => {
    const content = props.data
    return(
        <Popover
            placement='right'
        >
            <PopoverTrigger>
                {props.children}
            </PopoverTrigger>
            <PopoverContent>
                <PopoverArrow />
                {/* <PopoverCloseButton /> */}
                {/* <PopoverHeader>{content.headerText}</PopoverHeader> */}
                <PopoverBody>{content.bodyText}</PopoverBody>
            </PopoverContent>
        </Popover>
    )
    
}

export default MyPopover