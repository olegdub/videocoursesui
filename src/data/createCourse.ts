import { ILabelWithPopover } from "../components/TextBlocks/LabelWithPopover";

export const basePopovers: ILabelWithPopover[] = [
    {
        title: "Название курса",
        popover: {
            headerText: "",
            bodyText: "пример"
        }
    },
    {
        title: "Категории",
        popover: {
            headerText: "",
            bodyText: "пример"
        }
    },
    {
        title: "Превью",
        popover: {
            headerText: "",
            bodyText: "пример"
        }
    },
    {
        title: "Промо видео",
        popover: {
            headerText: "",
            bodyText: "пример"
        }
    },
    {
        title: "Описание",
        popover: {
            headerText: "",
            bodyText: "пример"
        }
    },
    {
        title: "Длительность видео",
        popover: {
            headerText: "",
            bodyText: "пример"
        }
    }
]

export const descriptionPopovers: ILabelWithPopover[] = [
    {
        title: "Заголовок блока",
        popover: {
            headerText: "",
            bodyText: "пример"
        }
    },
    {
        title: "Наполнение блока",
        popover: {
            headerText: "",
            bodyText: "пример"
        }
    },
]

export const modulePopovers: ILabelWithPopover[] = [
    {
        title: "Название модуля",
        popover: {
            headerText: "",
            bodyText: "пример"
        }
    },
    {
        title: "Краткое описание модуля",
        popover: {
            headerText: "",
            bodyText: "пример"
        }
    }
]

export const lessonPopovers: ILabelWithPopover[] = [
    {
        title: "Название урока",
        popover: {
            headerText: "",
            bodyText: "пример"
        }
    },
    {
        title: "Краткое описание урока",
        popover: {
            headerText: "",
            bodyText: "пример"
        }
    },
    {
        title: "Видео к уроку",
        popover: {
            headerText: "",
            bodyText: "пример"
        }
    },
    {
        title: "Текст урока",
        popover: {
            headerText: "",
            bodyText: "пример"
        }
    }
]

export const authorPopovers: ILabelWithPopover[] = [
    {
        title: "Полное имя автора",
        popover: {
            headerText: "",
            bodyText: "пример"
        }
    },
    {
        title: "Заслуги автора",
        popover: {
            headerText: "",
            bodyText: "пример"
        }
    },
    {
        title: "Стаж",
        popover: {
            headerText: "",
            bodyText: "пример"
        }
    }
]

export enum DescriptionVariants {
    base = "base"
}

export enum LessonVariants {
    video = "video"
}