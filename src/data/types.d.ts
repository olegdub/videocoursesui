
// стили блока описания курса
export default interface ICourseBodyStyles { 
    border: string, // граница
    bg: string, // цвет фона
    padding: string, // отступы (только для фронта)
}

// абзац текста 
export interface IParagraphItem {
    id: number
    text: string,
    type: string // тип: абзац или элемент списка - "text" или "icon"
}

// элемент списка (отличается иконкой)
export interface IPointItem extends IParagraphItem{
    icon: string
}

export type CourseBodyItems = IPointItem | IParagraphItem

export interface ICourseBody { // 1 блок с текстом для курса
    isAction: boolean,      // чисто для фронта
    position: number,       // порядок на странице
    title: string,          // заголовок блока
    bodyStyles: ICourseBodyStyles,  // стили блока (можно чисто на фронте оставить пока)
    items: CourseBodyItems[],       // элемент списка или абзац текста
}

// данные курса для продающей страницы
export interface ICourseBrand {
    courseId: number,       // id курса
    courseLink: string,     // ссылка на курс (для бека не нужна)
    preview: string,
    title: string,      // название курса
    rating: string,   // рейтинг курса
    students: number,   // количество купивших курс
    remarks: number,      // количество отзывов о курсе (можно массив всех отзывов хранить)
    description: string,  // краткое описание курса
    authorName: string,  // дублируется, но на фронте-то похуй
    dateUpdate: Date,
    courseBody: ICourseBody[] // тело курса текстовое описание
    aboutAuthor: {  // блок с автором курса
        avatar: string,
        authorName: string,
        items: CourseBodyItems[] // блоки описания автора
    }
}
