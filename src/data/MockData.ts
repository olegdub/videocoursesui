import { faker } from "@faker-js/faker"
import { ISimpleMenuItem } from "../components/Menu/SimpeMenuItem"

const getCatalogCategories = (): ISimpleMenuItem[] => {

    const data: ISimpleMenuItem[] = []

    const handleClick = (currentItemid: number): void => {
        console.log(currentItemid)
    }

    for (let i = 0; i < 25; i++) {
        data.push({
            id: i,
            text: faker.lorem.lines(1).slice(0, 20),
            link: `/category/${faker.lorem.lines(1).slice(0, 20)}`,
            handleClick
        })
        
    }

    return data
}