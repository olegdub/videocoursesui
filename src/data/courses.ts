import { faker } from "@faker-js/faker"
import ICourseBodyStyles, { ICourseBody, CourseBodyItems, ICourseBrand } from "./types"
import { randomNumberInRange } from "../utils"

export enum supportedIcons {
    'dot',
    'mark',
}

export enum supportedBodies {
    'icon' = 'point',
    'text' = 'paragraph'
}

export const bodyStyleVariants: ICourseBodyStyles[] = [
    { 
        border: '1px solid',
        bg: 'inherit',
        padding: '40px'
    },
    { 
        border: 'none',
        bg: 'inherit',
        padding: '0px'
    },
    { 
        border: 'none',
        bg: 'gray.200',
        padding: '40px'
    }
]

const getBodyItems = (isAction = false): CourseBodyItems[] => {

    const bodyItems: CourseBodyItems[] = []
    const itemsCount = randomNumberInRange(5,9)

    for (let i = 0; i < itemsCount; i++) {
        if (isAction || itemsCount % 2 !== 0) {
            let icon = faker.image.abstract(48, 48)
            if (!isAction && itemsCount % 3 === 0){
                icon = supportedIcons[1]
            }
            else if (!isAction && itemsCount === 7){
                icon = supportedIcons[0]
            }
            bodyItems.push({
                id: i,
                text: faker.lorem.lines(2),
                icon,
                type: supportedBodies.icon
            })
        }
        else {
            bodyItems.push({
                id: i,
                text: faker.lorem.lines(2),
                type: supportedBodies.text
            })
        }

        
    }

    return bodyItems
}

const getCourseBodies = (): ICourseBody[] => {
    const courseBody: ICourseBody[] = []

    for (let i = 0; i < 4; i++) {
        const bodyItems: CourseBodyItems[] = getBodyItems(i === 0)
        courseBody.push({
            isAction: i === 0,
            position: i,
            title: faker.lorem.lines(1),
            bodyStyles: bodyStyleVariants[randomNumberInRange(0,2)],
            items: bodyItems
        })
        
    }

    return courseBody
}


const getCoursesData = (coursesCount = 20): ICourseBrand[] => {

    const coursesData:ICourseBrand[] = []

    for (let i = 0; i < coursesCount; i++) {

        const authorItems: CourseBodyItems[] = getBodyItems()

        const author = faker.name.fullName()

        coursesData.push({
            courseId: i,
            courseLink: `/course/${i}`,
            title: faker.commerce.product.name,
            rating: "4.9",
            preview: '',
            students: Number(faker.commerce.price(1000, 20000, 0)),
            remarks: Number(faker.commerce.price(100, 2000, 0)),
            description: faker.lorem.lines(5),
            authorName: author,
            dateUpdate: new Date(faker.date.between('2020-01-01T00:00:00.000Z', '2023-01-01T00:00:00.000Z')),
            courseBody: getCourseBodies(),
            aboutAuthor: {
                avatar: faker.image.avatar(),
                authorName: author,
                items: authorItems
            }
        })
        
    }


    return coursesData
}

export default getCoursesData

export const separator = '$split$'