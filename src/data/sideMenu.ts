import { sitePaths } from "../axios"

export interface ISideMenuItem {
    id: number,
    link: string,
    text: string,
    isAuth: boolean 
}

const sideMenuData: ISideMenuItem[] = [
    {
        id: 1,
        link: sitePaths.userProfile,
        text: 'Настройки и подписка',
        isAuth: true
    },
    {
        id: 2,
        link: sitePaths.userCourses,
        text: 'Мои курсы',
        isAuth: true
    },
    {
        id: 3,
        link: "/",
        text: 'Главная',
        isAuth: false
    },
    {
        id: 4,
        link: sitePaths.catalog,
        text: 'Каталог курсов',
        isAuth: false
    },
    {
        id: 5,
        link: sitePaths.createCourse,
        text: 'Добавить курс',
        isAuth: true
    },

]

export const getDataForAll = (): ISideMenuItem[] => (
    sideMenuData.filter((item) => item.isAuth === false)
)

export const getDataForUser = (): ISideMenuItem[] => (
    sideMenuData
)

export default sideMenuData