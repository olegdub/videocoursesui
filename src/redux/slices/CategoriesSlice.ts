import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ICategoriesListResponse } from "../../types/responses";
import { ICategoryInList } from "../../types/catalog";
import { getHash, removeLastDirectoryPartOf } from "../../utils";
import { sitePaths } from "../../axios";

interface CategoriesState {
    categories: ICategoryInList[],
    loading: boolean,
    error: string
}

const initialState: CategoriesState = {
    categories: [],
    loading: false,
    error: ""
}

interface CategoryPayload {
    categories: ICategoriesListResponse[]
}

export const categoriesSlice = createSlice({
    name: 'categories',
    initialState,
    reducers: {
        categoriesLoading(state){
            const currentState = state
            currentState.loading = true
            currentState.error = ''
        },
        categoriesAddingSuccess(state, action: PayloadAction<CategoryPayload>){
            const currentState = state
            currentState.loading = false
            currentState.error = ''
            currentState.categories = action.payload.categories.map( c => {
                const link = removeLastDirectoryPartOf(sitePaths.categoryPage)
                return {
                id: getHash(c.cat_name),
                categoryName: c.cat_name,
                link: `${link}/${c.cat_name}`
            }})
        },
        categoriesError(state, action: PayloadAction<Error>){
            const currentState = state
            currentState.loading = false
            currentState.error = action.payload.message,
            currentState.categories = []
        }
    }
})

export default categoriesSlice.reducer