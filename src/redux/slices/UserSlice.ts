import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IUser } from "../../types/user";
import { IUserR, IUserResponse } from "../../types/responses";

interface UserState {
    user: IUser,
    loading: boolean,
    error: string
}

const initialState: UserState = {
    user: {
        id: 0,
        name: '',
        surname: '',
        email: ''
    },
    loading: false,
    error: ""
}

interface UserPayload {
    user: IUserR
}

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        userFetching(state){
            const currentState = state
            currentState.loading = true
        },
        userFetchingSuccess(state, action: PayloadAction<UserPayload>){
            const currentState = state
            currentState.loading = false
            currentState.error = ''
            const u = action.payload.user
            currentState.user = {
                id: u.user_id,
                name: u.fname,
                surname: u.lname,
                email: u.email,
            }
        },
        userFetchingError(state, action: PayloadAction<Error>){
            const currentState = state
            currentState.loading = false
            currentState.error = action.payload.message
        }
    }
})

export default userSlice.reducer