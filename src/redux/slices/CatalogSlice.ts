import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ICategoryDetailResponse, ICourseDemoResponse } from "../../types/responses";
import { ICourseCard } from "../../components/CourseCard/interfaces";
import { separator } from "../../data/courses";

interface CatalogState {
    popular: ICourseCard[],
    bigCourse: ICourseCard | null,
    newCourses: ICourseCard[],
    loading: boolean,
    error: string
}

const initialState: CatalogState = {
    popular: [],
    bigCourse: null,
    newCourses: [],
    loading: false,
    error: ""
}

interface CoursesPayload {
    category: ICategoryDetailResponse
}

interface OneCoursePayload {
    course: ICourseDemoResponse
}

export const catalogSlice = createSlice({
    name: 'catalog',
    initialState,
    reducers: {
        catalogLoading(state){
            const currentState = state
            currentState.loading = true
            currentState.error = ''
        },
        popularCoursesAddingSuccess(state, action: PayloadAction<CoursesPayload>){
            const currentState = state
            currentState.loading = false
            currentState.error = ''
            currentState.popular = action.payload.category.courses.map( c => {
                    return {
                        id: c.course.course_id,
                        image: c.course.preview_url.split(separator)[0],
                        title: c.course.title,
                        author: '',
                        // oldPrice: c.course.prod.origin_price,
                        // currentPrice: c.course.prod.price,
                        oldPrice: '',
                        currentPrice: '',
                        link: ''
                }})
            
            
        },
        newCoursesAddingSuccess(state, action: PayloadAction<CoursesPayload>){
            const currentState = state
            currentState.loading = false
            currentState.error = ''
            currentState.newCourses = action.payload.category.courses.map( c => {
                    return {
                        id: c.course.course_id,
                        image: c.course.preview_url,
                        title: c.course.title,
                        author: '',
                        // oldPrice: c.course.prod.origin_price,
                        // currentPrice: c.course.prod.price,
                        oldPrice: '',
                        currentPrice: '',
                        link: ''
                }})
            
            
        },

        bigCourseAddingSuccess(state, action: PayloadAction<OneCoursePayload>){
            const currentState = state
            currentState.loading = false
            currentState.error = ''
            const cd = action.payload.course.data
            currentState.bigCourse = {
                id: cd.course_id,
                image: cd.preview_url,
                title: cd.title,
                author: cd.author.user.username,
                oldPrice: cd.prod.origin_price,
                currentPrice: cd.prod.price,
                link: ''
            }
        },

        catalogError(state, action: PayloadAction<Error>){
            const currentState = state
            currentState.loading = false
            currentState.error = action.payload.message
        }
    }
})

export default catalogSlice.reducer