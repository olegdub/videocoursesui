import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ICategoriesListResponse, ICategoryDetailResponse } from "../../types/responses";
import { ICategoryDetail, ICategoryInList } from "../../types/catalog";
import { getHash, removeLastDirectoryPartOf } from "../../utils";
import { sitePaths } from "../../axios";

interface CategoryDetailState {
    category: ICategoryDetail,
    loading: boolean,
    error: string
}

const initialState: CategoryDetailState = {
    category: {
        id: 0,
        categoryName: '',
        link: '',
        courses: []
    },
    loading: false,
    error: ""
}

interface CategoryPayload {
    category: ICategoryDetailResponse
}

export const categoryDetailSlice = createSlice({
    name: 'categoryDetail',
    initialState,
    reducers: {
        categoryLoading(state){
            const currentState = state
            currentState.loading = true
            currentState.error = ''
        },
        categoryAddingSuccess(state, action: PayloadAction<CategoryPayload>){
            const currentState = state
            currentState.loading = false
            currentState.error = ''
            const link = removeLastDirectoryPartOf(sitePaths.categoryPage)
            currentState.category = {
                id: 0,
                categoryName: action.payload.category.cat_name,
                link: `${link}/${action.payload.category.cat_name}`,
                courses: action.payload.category.courses.map( c => {
                    return {
                        id: c.course.course_id,
                        image: c.course.preview_url,
                        title: c.course.title,
                        author: '',
                        // oldPrice: c.course.prod.origin_price,
                        // currentPrice: c.course.prod.price,
                        oldPrice: '',
                        currentPrice: '',
                        link: ''
                }})
            }
            
            
        },
        categoryError(state, action: PayloadAction<Error>){
            const currentState = state
            currentState.loading = false
            currentState.error = action.payload.message,
            currentState.category = {
                id: 0,
                categoryName: '',
                link: '',
                courses: []
            }
        }
    }
})

export default categoryDetailSlice.reducer