import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ICategoryDetailResponse, ICourseDemoResponse, ICoursePreview, ICoursePreviewResponse } from "../../types/responses";
import { ICourseCard } from "../../components/CourseCard/interfaces";

interface UserCoursesSliceState {
    myCourses: ICourseCard[],
    loading: boolean,
    error: string
}

const initialState: UserCoursesSliceState = {
    myCourses: [],
    loading: false,
    error: ""
}

interface CoursesPayload {
    courses: ICoursePreview[]
}


export const userCoursesSlice = createSlice({
    name: 'userCourses',
    initialState,
    reducers: {
        userCoursesLoading(state){
            const currentState = state
            currentState.loading = true
            currentState.error = ''
        },
        myCoursesAddingSuccess(state, action: PayloadAction<CoursesPayload>){
            const currentState = state
            currentState.loading = false
            currentState.error = ''
            console.log("store", action.payload.courses)
            currentState.myCourses = action.payload.courses.map( c => {
                    return {
                        id: c.course_id,
                        image: c.preview_url,
                        title: c.title,
                        author: '',
                        oldPrice: '',
                        currentPrice: '',
                        link: ''
                }})
            
            
        },

        userCoursesError(state, action: PayloadAction<Error>){
            const currentState = state
            currentState.loading = false
            currentState.error = action.payload.message
        }
    }
})

export default userCoursesSlice.reducer