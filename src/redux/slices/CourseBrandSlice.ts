import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ICourseDemoResponse, ICourseModule } from "../../types/responses";
import { ICourseBrand, ICourseBody, CourseBodyItems } from "../../data/types";
import getCoursesData, { bodyStyleVariants, separator, supportedBodies, supportedIcons } from "../../data/courses";
import { randomNumberInRange } from "../../utils";

interface CourseBrandSliceState {
    course: ICourseBrand,
    learnBlocks: ICourseModule[],
    video: string,
    loading: boolean,
    error: string
}

const initialState: CourseBrandSliceState = {
    course: getCoursesData(1)[0],
    video: '',
    learnBlocks: [],
    loading: false,
    error: ""
}

interface CoursesPayload {
    course: ICourseDemoResponse
}


export const courseBrandSlice = createSlice({
    name: 'curseBrand',
    initialState,
    reducers: {
        Loading(state){
            const currentState = state
            currentState.loading = true
            currentState.error = ''
        },
        courseAddingSuccess(state, action: PayloadAction<CoursesPayload>){
            const currentState = state
            currentState.loading = false
            currentState.error = ''
            
            const c = action.payload.course.data
            currentState.learnBlocks = c.learn_blocks
            let position = 0
            const courseBody: ICourseBody[] = c.text_blocks.map( (t, i) => {
                position += 1 
                if (i == 1) position += 1
                const textData = t.block.text.split(separator)
                const title = textData[0]
                const text = textData.length > 0 ? textData[1] : t.block.text
                return {
                    isAction: false,
                    position: position,
                    title,
                    bodyStyles: bodyStyleVariants[randomNumberInRange(0,2)],
                    items: [{
                        id: t.block.text_block_id,
                        text,
                        icon: '',
                        type: supportedBodies.text
                    }]
                }
            })
            courseBody.push({
                isAction: true,
                position: 2,
                title: c.prod.price + c.prod.origin_price,
                bodyStyles: bodyStyleVariants[randomNumberInRange(0,2)],
                items: [
                    {
                        id: 0,
                        text: `${c.learn_blocks.length} модулей обучения`,
                        icon: supportedIcons[0],
                        type: supportedBodies.icon
                    },
                    {
                        id: 1,
                        text: `полный доступ без ограничения по времени`,
                        icon: supportedIcons[0],
                        type: supportedBodies.icon
                    },
                    {
                        id: 2,
                        text: `удобный просмотр на любом устройстве`,
                        icon: supportedIcons[0],
                        type: supportedBodies.icon
                    },
                ]
            })
            const authorItems: CourseBodyItems[] = [
                {
                    id: c.author.author_id,
                    text: c.author.description,
                    type: supportedBodies.text
                }
            ]
            currentState.course = {
                courseId: c.course_id,  
                courseLink: '',
                preview: c.preview_url,  
                title: c.title,     
                rating: '',  
                students: 0,  
                remarks: 0,      
                description: '',  
                authorName: c.author.user.username,  
                dateUpdate: new Date(),
                courseBody: courseBody, 
                aboutAuthor: {  
                    avatar: '',
                    authorName: c.author.user.fname + c.author.user.lname,
                    items: authorItems 
                }
            }
            
            
        },

        Error(state, action: PayloadAction<Error>){
            const currentState = state
            currentState.loading = false
            currentState.error = action.payload.message
        }
    }
})

export default courseBrandSlice.reducer