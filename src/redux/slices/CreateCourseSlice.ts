import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ICategoryDetailResponse, ICourseDemoResponse } from "../../types/responses";
import { ICourseCard } from "../../components/CourseCard/interfaces";
import { ICourseState } from "../../types/admin";

interface CreateCourseState extends ICourseState {
    loading: boolean,
    error: string
}

const initialState: CreateCourseState = {
    modules: [],
    descriptions: [],
    loading: false,
    error: ""
}

interface CoursesPayload {
    category: ICategoryDetailResponse
}

export const createCourseSlice = createSlice({
    name: 'createCourse',
    initialState,
    reducers: {
        createCourseSuccess(state){
            const currentState = state
            currentState.loading = false
            currentState.error = ''
        },

        createCourseLoading(state){
            const currentState = state
            currentState.loading = true
            currentState.error = ''
        },

        createCourseError(state, action: PayloadAction<Error>){
            const currentState = state
            currentState.loading = false
            currentState.error = action.payload.message
        }
    }
})

export default createCourseSlice.reducer