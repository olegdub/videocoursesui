/* eslint-disable no-param-reassign */
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ITokenData } from "../../types/auth";

interface AuthState {
    isAuthenticated: boolean,
    token: ITokenData,
    loading: boolean,
    error: string
}

const ACCESS_TOKEN_KEY = 'dc-access'
const EXPIRES_KEY = 'dc-expires'
const REFRESH_TOKEN_KEY = 'dc-refresh'

export const EMPTY_TOKEN_DATA: ITokenData = {
    access_token: "qwrq",
    refresh_token: "",
    expires_in: Date()
}

function getInitialState(): AuthState {
    const expiresIn = localStorage.getItem(EXPIRES_KEY) ?? null

    if (expiresIn && new Date() > new Date(expiresIn)) {
        return {
            isAuthenticated: false,
            token: EMPTY_TOKEN_DATA,
            loading: false,
            error: ""
        }
    }

    return {
        isAuthenticated: Boolean(localStorage.getItem(ACCESS_TOKEN_KEY) ?? ''),
        token: {
            access_token: localStorage.getItem(ACCESS_TOKEN_KEY) ?? '',
            refresh_token: localStorage.getItem(REFRESH_TOKEN_KEY) ?? '',
            expires_in: localStorage.getItem(EXPIRES_KEY) ?? ''
        },
        loading: false,
        error: ""

    }
}

const initialState: AuthState = getInitialState()

interface AuthPayload {
    token: ITokenData
}
interface CheckAuthPayload {
    result: string
}

const errorHandler = (error: string): string => {
    switch (error) {
        case "Network error":
            return "Неудалось установить соединение, проверьте интернет"

        case "Request failed with status code 401":
            return "Неверный логин или пароль"
        case "user info with this username already exists.":
            return "Пользователь уже существует"
        case "user info with this email already exists.":
            return "Почте уже привязан аккаунт"
        case "This password is too common.":
            return "Пароль должен содержать хотя бы 1 специальный символ и 1 заглавную букву "
        default:
            return "Что-то пошло не так, попробуйте снова"
    }
}

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        authValidate(state){
            state.loading = true
            state.error = ""
        },
        authSetError(state, action: PayloadAction<Error>){
            state.loading = false
            state.error = errorHandler(action.payload.message)
        },
        logout(state) {
            state.loading = false

            state.isAuthenticated = false
            state.token = EMPTY_TOKEN_DATA
            localStorage.removeItem(ACCESS_TOKEN_KEY)
            localStorage.removeItem(REFRESH_TOKEN_KEY)
            localStorage.removeItem(EXPIRES_KEY)

        },
        loginSuccess(state, action: PayloadAction<AuthPayload>) {
            state.loading = false

            state.isAuthenticated = Boolean(action.payload.token.access_token)
            state.token = action.payload.token

            localStorage.setItem(ACCESS_TOKEN_KEY, action.payload.token.access_token)
            localStorage.setItem(EXPIRES_KEY, action.payload.token.expires_in)
            localStorage.setItem(REFRESH_TOKEN_KEY, action.payload.token.refresh_token)
        },
        registrationSuccess(state) {
            state.loading = false
            state.isAuthenticated = false
            state.error = ''
        },
        checkAuth(state, action: PayloadAction<CheckAuthPayload>){
            state.loading = false

            state.isAuthenticated = action.payload.result === 'ok'

            if (action.payload.result !== 'ok'){
                state.isAuthenticated = false
                state.token = EMPTY_TOKEN_DATA
                localStorage.removeItem(ACCESS_TOKEN_KEY)
                localStorage.removeItem(REFRESH_TOKEN_KEY)
                localStorage.removeItem(EXPIRES_KEY)
            }
            else {
                state.isAuthenticated = true
            }
        }
    }
})

export default authSlice.reducer