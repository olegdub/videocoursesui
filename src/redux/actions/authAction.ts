import { AxiosError } from "axios"
import axios, { apiUrls } from "../../axios"
import { ILoginValue, IRegisterResponse, IRegisterValue, ITokenData } from "../../types/auth"
import { EMPTY_TOKEN_DATA, authSlice } from "../slices/AuthSlice"
import { AppDispatch, RootState } from "../store"


export const login = (data: ILoginValue) => (
    async (dispatch: AppDispatch) => {
        try {
            dispatch(authSlice.actions.authValidate())
            // sending request
            const response = await axios.post<IRegisterResponse>(
                apiUrls.createToken, 
                data
            )

            dispatch(authSlice.actions.loginSuccess({
                token: {
                    access_token: response.data.data.access,
                    refresh_token: response.data.data.refresh,
                    expires_in: Date()
                }
            }))
        } catch (e) {
            dispatch(authSlice.actions.authSetError(e as Error))
        }
    }
)

export const loginTest = (data: ILoginValue) => (
    async (dispatch: AppDispatch) => {
        try {
            dispatch(authSlice.actions.authValidate())
            // sending request
            // const response = await axios.post<ITokenData>('auth/login', data)

            dispatch(authSlice.actions.loginSuccess({
                token: EMPTY_TOKEN_DATA
            }))
        } catch (e) {
            dispatch(authSlice.actions.authSetError(e as Error))
        }
    }
)

export const regTest = (data: IRegisterValue) => (
    async (dispatch: AppDispatch) => {
        try {
            dispatch(authSlice.actions.authValidate())
            // sending request
            // const response = await axios.post<ITokenData>('auth/login', data)

            dispatch(authSlice.actions.loginSuccess({
                token: EMPTY_TOKEN_DATA
            }))
        } catch (e) {
            dispatch(authSlice.actions.authSetError(e as Error))
        }
    }
)

export const registration = (data: IRegisterValue) => (
    async (dispatch: AppDispatch) => {
        try {
            dispatch(authSlice.actions.authValidate())
            // sending request
            const response = await axios.post(apiUrls.registration, data)

            console.log(response.status)

            const response2 = await axios.post<IRegisterResponse>(apiUrls.createToken, {
                email: data.email,
                password: data.password
            })

            console.log(response2.status)
            console.log(response2.data)

            dispatch(authSlice.actions.loginSuccess({
                    token: {
                        access_token: response2.data.data.access,
                        refresh_token: response2.data.data.refresh,
                        expires_in: Date()
                    }
                }
            ))

            // dispatch(authSlice.actions.registrationSuccess())
        } catch (e) {
            dispatch(authSlice.actions.authSetError(e as Error))
        }
    }
)

export const createToken = (data: ILoginValue) => (
    async (dispatch: AppDispatch) => {
        try {
            dispatch(authSlice.actions.authValidate())
            // sending request
            const response = await axios.post<IRegisterResponse>(apiUrls.createToken, data)

            console.log(response.status)

            dispatch(authSlice.actions.loginSuccess({
                    token: {
                        access_token: response.data.data.access,
                        refresh_token: response.data.data.refresh,
                        expires_in: Date()
                    }
                }
            ))
        } catch (e) {
            dispatch(authSlice.actions.authSetError(e as Error))
        }
    }
)

export const refreshToken = () => (
    async (dispatch: AppDispatch, getState: () => RootState) => {
        try {
            dispatch(authSlice.actions.authValidate())

            const refreshToken = getState().auth.token.refresh_token
            // sending request
            const response = await axios.post<
                {
                    data: {
                        access: string
                    }
                }
            >(
                apiUrls.refreshToken, 
                {refresh: refreshToken}
            )

            console.log(response.status)

            dispatch(authSlice.actions.loginSuccess({
                    token: {
                        access_token: response.data.data.access,
                        refresh_token: refreshToken,
                        expires_in: Date()
                    }
                }
            ))
        } catch (e) {
            dispatch(authSlice.actions.authSetError(e as Error))
        }
    }
)

export const logout = () => (
    async (dispatch: AppDispatch) => {
        try {
            dispatch(authSlice.actions.authValidate())
            dispatch(authSlice.actions.logout())
        } catch (e) {
            dispatch(authSlice.actions.authSetError(e as Error))
        }
    }
)


export const checkAuth = () => (
    async (dispatch: AppDispatch, getState: () => RootState) => {
        try {
            dispatch(authSlice.actions.authValidate())

            const access = getState().auth.token.access_token
            
            if (access === '') {
                dispatch(authSlice.actions.checkAuth(
                    {result: 'no'}
                ))
                return
            }

            const response = await axios.get<{result: string}>('auth', {
                headers: {
                    Authorization: `JWT ${access}`
                }
            })

            dispatch(authSlice.actions.checkAuth(
                response.data
            ))

        } catch (e) {
            dispatch(authSlice.actions.checkAuth(
                {result: 'no'}
            ))
        }
    }
)
