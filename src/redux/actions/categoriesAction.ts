import axios, { apiUrls } from "../../axios"
import { ICategoriesListResponse, ICategoryDetailResponse } from "../../types/responses"
import { categoriesSlice } from "../slices/CategoriesSlice"
import CategoryDetailSlice, { categoryDetailSlice } from "../slices/CategoryDetailSlice"
import { AppDispatch } from "../store"


export const getCategories = () => (
    async (dispatch: AppDispatch) => {
        try {
            dispatch(categoriesSlice.actions.categoriesLoading())
            // sending request
            const response = await axios.get<ICategoriesListResponse[]>(apiUrls.getCategories,
                {
                    headers: {
                        Authorization: ``,
                        'Content-Type': 'application/json'
                    }
                }    
            )

            dispatch(categoriesSlice.actions.categoriesAddingSuccess({
                // @ts-ignore
                categories: response.data.data
            }))
        } catch (e) {
            dispatch(categoriesSlice.actions.categoriesError(e as Error))
        }
    }
)

export const getCategoryDetail = (categoryName: string) => (
    async (dispatch: AppDispatch) => {
        try {
            dispatch(categoryDetailSlice.actions.categoryLoading())
            // sending request
            const response = await axios.get<ICategoryDetailResponse>(
                `${apiUrls.getCategoryDetail}${categoryName}`,
                {
                    headers: {
                        Authorization: ``,
                        'Content-Type': 'application/json'
                    }
                }   
            )
            // debugger
            dispatch(categoryDetailSlice.actions.categoryAddingSuccess({
                // @ts-ignore
                category: response.data.data
            }))
        } catch (e) {
            dispatch(categoryDetailSlice.actions.categoryError(e as Error))
        }
    }
)