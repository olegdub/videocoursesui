import axios, { apiUrls } from "../../axios"
import { ICategoryDetailResponse, ICourseDemoResponse, ICoursePreviewResponse } from "../../types/responses"
import { courseBrandSlice } from "../slices/CourseBrandSlice"
import { userCoursesSlice } from "../slices/UserCoursesSlice"
import { AppDispatch, RootState } from "../store"



export const getCourseBrand = (id: number) => (
    async (dispatch: AppDispatch, getState: () => RootState) => {
        try {
            dispatch(courseBrandSlice.actions.Loading())
            // sending request
            const response = await axios.get<ICourseDemoResponse>(
                `${apiUrls.courseBase}${id}${apiUrls.getCourseDemo}`,
                {
                    headers: {
                        Authorization: ``,
                        'Content-Type': 'application/json'
                    }
                }
            )
            // debugger
            dispatch(courseBrandSlice.actions.courseAddingSuccess({
                
                course: response.data
            }))
        } catch (e) {
            dispatch(courseBrandSlice.actions.Error(e as Error))
        }
    }
)