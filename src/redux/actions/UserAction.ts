import { AxiosError } from "axios"
import axios, { apiUrls } from "../../axios"
import { IUserResponse } from "../../types/responses"
import { ICreateUser, IUpdateUser, IUser } from "../../types/user"
import { userSlice } from "../slices/UserSlice"
import { AppDispatch, RootState } from "../store"

export const getUser = () => (
    async (dispatch: AppDispatch, getState: () => RootState) => {
        try {
            dispatch(userSlice.actions.userFetching())
            const access = getState().auth.token.access_token
            // send request
            const response = await axios.get<IUserResponse>(apiUrls.getUser, {
                headers: {
                    Authorization: `JWT ${access}`,
                    'Content-Type': 'application/json'
                }
            })
            
            dispatch(userSlice.actions.userFetchingSuccess({
                user: response.data.data
            }))
        } catch (e) {
            dispatch(userSlice.actions.userFetchingError(e as Error))
        }
    }
)

// for org
// todo: change success dispatches

export const updateUser = (userId: number, userNewData: IUpdateUser) => (
    async (dispatch: AppDispatch, getState: () => RootState) => {
        try {
            dispatch(userSlice.actions.userFetching())
            const access = getState().auth.token.access_token
            // send request
            const response = await axios.put<IUserResponse>(`user/${userId}`,
                userNewData,
                {headers: {
                        Authorization: `JWT ${access}`,
                        'Content-Type': 'application/json'
                    }
                })
            dispatch(userSlice.actions.userFetchingSuccess({
                user: response.data.data
            }))
        } catch (e) {
            dispatch(userSlice.actions.userFetchingError(e as Error))
        }
    }
)
