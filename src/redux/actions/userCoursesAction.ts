import axios, { apiUrls } from "../../axios"
import { ICategoryDetailResponse, ICoursePreviewResponse, IMyCoursesResponse } from "../../types/responses"
import { userCoursesSlice } from "../slices/UserCoursesSlice"
import { AppDispatch, RootState } from "../store"



export const getMyCourses = () => (
    async (dispatch: AppDispatch, getState: () => RootState) => {
        try {
            dispatch(userCoursesSlice.actions.userCoursesLoading())
            const access = getState().auth.token.access_token
            // sending request
            const response = await axios.get<IMyCoursesResponse>(
                `${apiUrls.getUserCourses}`,
                {
                    headers: {
                        "Authorization": `JWT ${access}`,
                        'Content-Type': 'application/json'
                    }
                }
            )
            dispatch(userCoursesSlice.actions.myCoursesAddingSuccess({
                
                courses: response.data.data
            }))
        } catch (e) {
            dispatch(userCoursesSlice.actions.userCoursesError(e as Error))
        }
    }
)