import axios, { apiUrls } from "../../axios"
import { CourseBaseFormValue } from "../../components/Forms/admin/CreateCourseBase"
import { separator } from "../../data/courses"
import { IAddCourseCategory, IAddCourseModule, IAddCourseTextBlock, IAddLessonVideo, ICourseState, ICreateCourseData, ICreateCourseLesson, IModule, IModuleLesson } from "../../types/admin"
import { ICourseCreateLessonResponse, ICourseCreateModuleResponse, ICourseCreateResponse, IVideoUploadKinescopeResponse } from "../../types/responses"
import { createCourseSlice } from "../slices/CreateCourseSlice"
import { AppDispatch, RootState } from "../store"


export const createCourse = (data: ICreateCourseData) => (
    async (dispatch: AppDispatch, getState: () => RootState) => {
        try {
            dispatch(createCourseSlice.actions.createCourseLoading())
            const access = getState().auth.token.access_token
            // sending request

            const response = await axios.post(
                `${apiUrls.courseBase}`,
                data,
                {
                    headers: {
                        Authorization: `JWT ${access}`,
                        'Content-Type': 'application/json'
                    }
                }
            )
            // debugger
            dispatch(createCourseSlice.actions.createCourseSuccess())
        } catch (e) {
            dispatch(createCourseSlice.actions.createCourseError(e as Error))
        }
    }
)

export const addCourseCategory = (data: IAddCourseCategory) => (
    async (dispatch: AppDispatch, getState: () => RootState) => {
        try {
            dispatch(createCourseSlice.actions.createCourseLoading())
            const access = getState().auth.token.access_token
            // sending request
            const link =  `\
            ${apiUrls.courseBase}\
            ${data.courseId}\
            ${apiUrls.addCourseCategory_name}\
            ${data.name}\
            ${apiUrls.addCourseCategory_action}\
            add\
            ${apiUrls.addCourseCategory_isMain}\
            ${data.isMain}\
            `
            const response = await axios.get(
                link,
                {
                    headers: {
                        Authorization: `JWT ${access}`,
                        'Content-Type': 'application/json'
                    }
                }
            )
            // debugger
            dispatch(createCourseSlice.actions.createCourseSuccess())
        } catch (e) {
            dispatch(createCourseSlice.actions.createCourseError(e as Error))
        }
    }
)

export const addCourseTextBlock = (courseId: number, data: IAddCourseTextBlock) => (
    async (dispatch: AppDispatch, getState: () => RootState) => {
        try {
            dispatch(createCourseSlice.actions.createCourseLoading())
            const access = getState().auth.token.access_token
            // sending request
            const link =  `\
            ${apiUrls.courseBase}\
            ${courseId}\
            ${apiUrls.addCourseTextBlock}`

            const response = await axios.post(
                link,
                data,
                {
                    headers: {
                        Authorization: `JWT ${access}`,
                        'Content-Type': 'application/json'
                    }
                }
            )
            // debugger
            dispatch(createCourseSlice.actions.createCourseSuccess())
        } catch (e) {
            dispatch(createCourseSlice.actions.createCourseError(e as Error))
        }
    }
)

export const addCourseModule = (data: IAddCourseModule) => (
    async (dispatch: AppDispatch, getState: () => RootState) => {
        try {
            dispatch(createCourseSlice.actions.createCourseLoading())
            const access = getState().auth.token.access_token
            // sending request
            const link =  `${apiUrls.addCourseModule}`

            const response = await axios.post(
                link,
                data,
                {
                    headers: {
                        Authorization: `JWT ${access}`,
                        'Content-Type': 'application/json'
                    }
                }
            )
            // debugger
            dispatch(createCourseSlice.actions.createCourseSuccess())
        } catch (e) {
            dispatch(createCourseSlice.actions.createCourseError(e as Error))
        }
    }
)

export const createCourseLesson = (data: ICreateCourseLesson) => (
    async (dispatch: AppDispatch, getState: () => RootState) => {
        try {
            dispatch(createCourseSlice.actions.createCourseLoading())
            const access = getState().auth.token.access_token
            // sending request
            const link =  `${apiUrls.courseLesson}`

            const response = await axios.post(
                link,
                data,
                {
                    headers: {
                        Authorization: `JWT ${access}`,
                        'Content-Type': 'application/json'
                    }
                }
            )
            // debugger
            dispatch(createCourseSlice.actions.createCourseSuccess())
        } catch (e) {
            dispatch(createCourseSlice.actions.createCourseError(e as Error))
        }
    }
)

export const AddLessonVideo = (lessonId:number, data: IAddLessonVideo) => (
    async (dispatch: AppDispatch, getState: () => RootState) => {
        try {
            dispatch(createCourseSlice.actions.createCourseLoading())
            const access = getState().auth.token.access_token
            // sending request
            const link =  `\
            ${apiUrls.courseLesson}\
            ${lessonId}\
            ${apiUrls.addCourseLessonVideo}\
            `

            const response = await axios.post(
                link,
                data,
                {
                    headers: {
                        Authorization: `JWT ${access}`,
                        'Content-Type': 'application/json'
                    }
                }
            )
            // debugger
            dispatch(createCourseSlice.actions.createCourseSuccess())
        } catch (e) {
            dispatch(createCourseSlice.actions.createCourseError(e as Error))
        }
    }
)

export const createCourseFull = (courseState: ICourseState, courseData: CourseBaseFormValue) => (
    async (dispatch: AppDispatch, getState: () => RootState) => {
        try {
            dispatch(createCourseSlice.actions.createCourseLoading())
            const access = getState().auth.token.access_token

            const apiToken = '6a07261c-71b2-4388-b68a-c4b8ef548ab6'

            const kinescopeBase = await axios.post<IVideoUploadKinescopeResponse>(
                'https://uploader.kinescope.io/video',
                courseData.intro,
                {
                    headers: {
                        'Authorization': `Bearer ${apiToken}`,
                        'Content-Type': 'application/json',
                        'X-File-Name': `${courseData.intro?.name}`,
                        'X-Video-Title': `${courseData.title}`,
                        'X-Video-Description': `${courseData.description}`
                    }
                }
            )
            let k = kinescopeBase.data.data
            const previewLink = `${k.poster}${separator}${k.embed_link}`
            console.log(kinescopeBase)

            const createCourseData: ICreateCourseData = {
                title: courseData.title,
                // @ts-ignore
                preview_url: previewLink,
                price: 2000
            }

            const responseCreateCourse = await axios.post<ICourseCreateResponse>(
                `${apiUrls.courseBase}`,
                createCourseData,
                {
                    headers: {
                        Authorization: `JWT ${access}`,
                        'Content-Type': 'application/json'
                    }
                }
            )
            
        
            const courseId = responseCreateCourse.data.data.course_id
            
            console.log('created course', courseId)

            const addCourseCategories: 
                IAddCourseCategory[] = courseData.categories.map(c => (
                {
                    courseId,
                    name: c, 
                    isMain: 0
                }
            ))
            addCourseCategories[0].isMain = 1

            addCourseCategories.forEach( c => {
                const linkCategories =  `${apiUrls.courseBase}${courseId}${apiUrls.addCourseCategory_name}${c.name}${apiUrls.addCourseCategory_action}add${apiUrls.addCourseCategory_isMain}${c.isMain}`
                axios.put(
                    linkCategories,
                    {},
                    {
                        headers: {
                            Authorization: `JWT ${access}`,
                            'Content-Type': 'application/json'
                        }
                    }
                )
                console.log('add category', c.name)
            })
        
            const addCourseTextBlocks: 
                IAddCourseTextBlock[] = courseState.descriptions.map( d => (
                    {
                        text: d.formValue.title + separator + d.formValue.description,
                        type: 'Абзац',
                        order: d.element.order,
                    }
                ))
            addCourseTextBlocks.forEach( t => {
                const linkTextBlock =  `${apiUrls.courseBase}${courseId}${apiUrls.addCourseTextBlock}`

                axios.post(
                    linkTextBlock,
                    t,
                    {
                        headers: {
                            Authorization: `JWT ${access}`,
                            'Content-Type': 'application/json'
                        }
                    }
                )
                console.log('add text block', t.text)
            })

            const getModuleFromState = (id: number): IModule => {
                return courseState.modules.filter( m => m.id === id)[0]
            }

            const getLessonFromModule = (id:number, moduleId: number): IModuleLesson => {
                return getModuleFromState(moduleId).element.lessons.filter( l => l.id === id)[0]
            }

            const addCourseModules: 
                {
                    id: number,
                    data: IAddCourseModule
                }[] = courseState.modules.map( m=> (
                    {
                        id: m.id,
                        data: {
                            order: m.element.order,
                        title: m.formValue.title,
                        description: m.formValue.description,
                        course: courseId
                        }
                    }
                ))
            
            addCourseModules.forEach( async m => {
                const link =  `${apiUrls.addCourseModule}`

                const responseModule = await axios.post<ICourseCreateModuleResponse>(
                    link,
                    m.data,
                    {
                        headers: {
                            Authorization: `JWT ${access}`,
                            'Content-Type': 'application/json'
                        }
                    }
                )

                console.log('add module', m.data.title)

                const currentModule = getModuleFromState(m.id)

                const createCourseLessons: 
                {
                    id: number,
                    data: ICreateCourseLesson
                }[] = currentModule.element.lessons.map(l => (
                    {
                        id: l.id,
                        data: {
                            order: l.element.order,
                            title: l.formValue.title,
                            description: l.formValue.description,
                            learn_block: responseModule.data.data.learn_block_id
                        }
                    }
                ))

                createCourseLessons.forEach( async l => {
                    const linkLesson =  `${apiUrls.courseLesson}`

                    const responseLesson = await axios.post<ICourseCreateLessonResponse>(
                        linkLesson,
                        l.data,
                        {
                            headers: {
                                Authorization: `JWT ${access}`,
                                'Content-Type': 'application/json'
                            }
                        }
                    )
                    
                    console.log('add lesson', l.data.title)

                    const currentLesson = getLessonFromModule(l.id, m.id)

                    const linkLessonVideo =  `${apiUrls.courseLesson}${responseLesson.data.data.lesson_id}${apiUrls.addCourseLessonVideo}`

                    ///
                    // @ts-ignore
                    const cv = currentLesson.formValue.video.file

                    const requestKinescope = await axios.post<IVideoUploadKinescopeResponse>(
                        'https://uploader.kinescope.io/video',
                        cv,
                        {
                            headers: {
                                'Authorization': `Bearer ${apiToken}`,
                                'Content-Type': 'application/json',
                                'X-File-Name': `${cv?.name}`,
                                'X-Video-Title': `${currentLesson.formValue.title}`,
                                'X-Video-Description': `${currentLesson.formValue.description}`
                            }
                        }
                    )

                    console.log('send video ', requestKinescope)
                    const addLessonVideo: IAddLessonVideo = {
                        video_id_related: requestKinescope.data.data.id,
                        url: requestKinescope.data.data.embed_link,
                        title: currentLesson.formValue.title,
                        description: currentLesson.formValue.description,
                        length: '11:22:00',
                        type: 'video'
                    }
                    ///

                    axios.post(
                        linkLessonVideo,
                        addLessonVideo,
                        {
                            headers: {
                                Authorization: `JWT ${access}`,
                                'Content-Type': 'application/json'
                            }
                        }
                    )
                    console.log('add video to lesson', addLessonVideo.title)
                })
            })
            dispatch(createCourseSlice.actions.createCourseSuccess())
        } catch (e) {
            dispatch(createCourseSlice.actions.createCourseError(e as Error))
        }
    }
)


