import axios, { apiUrls } from "../../axios"
import { ICategoryDetailResponse, ICourseDemoResponse } from "../../types/responses"
import { catalogSlice } from "../slices/CatalogSlice"
import { AppDispatch } from "../store"


const categoryNameOfPopular = "Популярные"
const categoryNameOfNew = "Новые"
const bigCourseId = 3

export const getPopularCourses = () => (
    async (dispatch: AppDispatch) => {
        try {
            dispatch(catalogSlice.actions.catalogLoading())
            // sending request
            const response = await axios.get<ICategoryDetailResponse>(
                `${apiUrls.getCategoryDetail}${categoryNameOfPopular}`,
                {
                    headers: {
                        Authorization: ``
                    }
                }
            )
            // debugger
            dispatch(catalogSlice.actions.popularCoursesAddingSuccess({
                // @ts-ignore
                category: response.data.data
            }))
        } catch (e) {
            dispatch(catalogSlice.actions.catalogError(e as Error))
        }
    }
)

export const getBigCourse = () => (
    async (dispatch: AppDispatch) => {
        try {
            dispatch(catalogSlice.actions.catalogLoading())
            // sending request
            const response = await axios.get<ICourseDemoResponse>(
                `${apiUrls.courseBase}${bigCourseId}${apiUrls.getCourseDemo}`,
                {
                    headers: {
                        Authorization: ``
                    }
                }
                
            )
            dispatch(catalogSlice.actions.bigCourseAddingSuccess({
               
                course: response.data
            }))
        } catch (e) {
            dispatch(catalogSlice.actions.catalogError(e as Error))
        }
    }
)

export const getNewCourses = () => (
    async (dispatch: AppDispatch) => {
        try {
            dispatch(catalogSlice.actions.catalogLoading())
            // sending request
            const response = await axios.get<ICategoryDetailResponse>(
                `${apiUrls.getCategoryDetail}${categoryNameOfNew}`,
                {
                    headers: {
                        Authorization: ``
                    }
                }
            )
            // debugger
            dispatch(catalogSlice.actions.newCoursesAddingSuccess({
                // @ts-ignore
                category: response.data.data
            }))
        } catch (e) {
            dispatch(catalogSlice.actions.catalogError(e as Error))
        }
    }
)