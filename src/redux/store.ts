import { combineReducers, configureStore } from "@reduxjs/toolkit";
import authReducer from './slices/AuthSlice'
import categoriesReducer from './slices/CategoriesSlice'
import userReducer from './slices/UserSlice'
import categoryDetailReducer from './slices/CategoryDetailSlice'
import catalogReducer from './slices/CatalogSlice'
import CreateReducer from "./slices/CreateCourseSlice"
import userCoursesReducer from './slices/UserCoursesSlice'
import courseBrandSlice from './slices/CourseBrandSlice'

const rootReducer = combineReducers({
    auth: authReducer,
    user: userReducer,
    categories: categoriesReducer,
    categoryDetail: categoryDetailReducer,
    catalog: catalogReducer,
    createCourse: CreateReducer,
    userCourses: userCoursesReducer,
    courseBrand: courseBrandSlice
})

export default function setupStore() {
    return configureStore({
        reducer: rootReducer,
        middleware: (getDefaultMiddleware) => getDefaultMiddleware({
            serializableCheck: false,
          }),
    })
}


export type RootState = ReturnType<typeof rootReducer>
export type AppStore = ReturnType<typeof setupStore>
export type AppDispatch = AppStore['dispatch']