import axios from "axios";
import { refreshToken } from "../redux/actions/authAction";

export const BASE_URL = 'http://127.0.0.1:8000/'
// export const BASE_URL = 'http://62.113.103.188/'

export enum sitePaths {
    home = '/',
    forgotPassword = '/change-password',
    login = '/login',
    register = '/registration',
    logout = '/logout',
    courseBrand = '/course/:courseId',// also add a /:courseId
    categoryPage = '/category/:categoryId',   
    catalog = '/catalog',
    courseView = '/user/course/:courseId',  // also add a /:courseId
    userProfile = '/user/profile',
    userCourses = '/user/courses',
    buyPage = '/buy',
    error404 = '/not-found',
    success = '/success',
    createCourse = '/admin/course/add'
}

export enum apiUrls {
    // post
    registration = 'auth/users/',
    createToken = 'auth/jwt/create/',
    refreshToken = 'auth/jwt/refresh/',
    getUser = 'auth/users/me/',
    getCategories = 'api/category/all',
    getCategoryDetail = 'api/category/one?category_name=',
    getCourseDemo = '/demo/',
    getCourseFull = '/full/',
    courseBase = 'api/course/',
    addCourseCategory_name = '/category/?category_name=',
    addCourseCategory_action = '&action=',
    addCourseCategory_isMain = '&is_main=',
    addCourseTextBlock = '/description/',
    addCourseModule = 'api/learnblock/',
    courseLesson = 'api/lesson/',
    addCourseLessonVideo = '/video/',
    getUserCourses = 'api/user/course/bought/',

}

const apiClient = axios.create({
    baseURL: BASE_URL
})

/* ==$API with  response interceptors== */

export function refresh(token: string) {
    return axios.post<{ refresh: string }>(
        apiUrls.refreshToken,
        { refresh: token },
        {
            withCredentials: true,
        }
    )
}

apiClient.interceptors.response.use(
    (config) => config,
    async (error) => {
        const originalRequest = error.config
        if (
            error.response.status === 401 &&
            error.config &&
            !error.config._isRetry
        ) {
            originalRequest._isRetry = true
            try {
                const token = localStorage.getItem('refresh')
                refresh(token || '')
                    .then((response) => {
                        localStorage.setItem('refresh', response.data.refresh)
                        refreshToken()
                    })
                    .catch(() => {
                        localStorage.removeItem('refresh')
                    })
                return await apiClient.request(originalRequest)
            } catch (e) {
                // eslint-disable-next-line
                console.log(e)
            }
        }
        throw error
    }
)

export default apiClient