import { Box, Container, Flex, Heading, Icon, Image, Spinner, Text } from "@chakra-ui/react"
import { useEffect, useRef, useState } from "react"
import Header from "../components/Header/Header"
import Footer from "../sections/Footer/Footer"
import { ICourseBrand } from "../data/types"
import getCoursesData, { separator } from "../data/courses"
import CourseBody from "../components/CourseBodies/CourseBody"
import { AtSignIcon, RepeatClockIcon, StarIcon } from "@chakra-ui/icons"
import LearningPlan from "../sections/LearningPlan/LearningPlan"
import { colors } from "../theme"
import shieldIcon from '../assets/images/shield-3.svg'
import FAQ from "../sections/FAQ/FAQ"
import { useAppDispach, useAppSelector } from "../hooks/redux"
import { useLocation } from "react-router-dom"
import { getCourseBrand } from "../redux/actions/courseBrandAction"
import { faker } from "@faker-js/faker"



const CourseBrandPage = (): JSX.Element => {

    const [headingHeight, setHeadingHeight] = useState(0)
    const dispatch = useAppDispach()
    const { course } = useAppSelector(state => state.courseBrand)

    const courseId = useLocation().pathname.split('/').pop()

    useEffect(() => {
        dispatch(getCourseBrand(Number(courseId)))
    }, [courseId])

    const ref = useRef(null)

    useEffect(() => {
        // @ts-ignore
        setHeadingHeight(ref.current.clientHeight)
        
      })

    if( course === undefined) return (
        <Box
            height='100%'
            display='flex'
            flexDir='column'
        >
            <Box ref={ref}>
                <Header />
            </Box>
                
            <Box
                flex='1 0'
            >
                <Container 
                    maxW={{base: 'calc(100% - 14px)', md: '100%', xl: '660px'}}
                    mt='60px'
                >
                    <Spinner/>
                </Container>
                </Box>

            <Footer/>
        </Box>
    )
    console.log(course.courseBody)
    const courseBody = [...course.courseBody]
    courseBody.sort((a, b) => {
        if (a.position < b.position) {
            return -1;
          }
          if (a.position > b.position) {
            return 1;
          }
          return 0;
    })
    const courseBodies = courseBody.map( cb => (
        <CourseBody key={cb.position} body={cb} courseId={course.courseId}/>
    ))

    const courseUpdateDate = `${course.dateUpdate.getDate()} ${course.dateUpdate.getFullYear()}`

    return (
        <Box
            height='100%'
            display='flex'
            flexDir='column'
        >
            <Box ref={ref}>
                <Header />
            </Box>
                
            <Box
                flex='1 0'
            >
                <Container 
                    maxW={{base: 'calc(100% - 14px)', md: '100%', xl: '660px'}}
                    mt='60px'
                >

                    {/* player */}
                    <Flex
                        flexDir='column'
                    >
                         <Box
                            overflow='hidden'
                            borderRadius='25px'
                        >
                            <div style={{"position": "relative", "paddingTop": "56.25%", "width": "100%"}}>
                                <iframe 
                                src={course.preview.split(separator)[1]}
                                allow="autoplay; fullscreen; picture-in-picture; encrypted-media;" 
                                frameBorder="0" allowFullScreen style={{"position": "absolute", "width": "100%", "height": "100%", "top": "0", "left": "0"}}/>
                            </div>
                        </Box>

                        <Heading
                            as='h2' fontSize='32'
                        >
                            
                        </Heading>


                    </Flex>

               
                    <Flex
                        flexDir="column"
                        my='40px'
                    >
                        {/* course preview */}
                        <Flex
                            flexDir="column"
                            gap='20px'
                            my='40px'
                        >
                            <Heading as='h1' fontSize='28px'>
                                {course.title}
                            </Heading>

                            {/* ratings */}
                            <Flex
                                alignItems='center'
                            >
                                <Icon
                                    as={StarIcon}
                                    color='yellow'
                                    w='28px'
                                    h='28px'
                                    mr='15px'
                                />

                                <Text
                                    fontSize='22px'
                                >   
                                    {course.rating.length > 0 ? (
                                        course.rating
                                    ) : (
                                        "0.0"
                                    )}
                                </Text>
                                

                            </Flex>

                            {/* description */}
                            <Text>
                                {course.description}
                            </Text>

                            {/* author */}
                            <Flex
                                alignItems='center'
                            >
                                <Icon
                                    as={AtSignIcon}
                                    color='gray.400'
                                    w='24px'
                                    h='24px'
                                    mr='15px'
                                />

                                <Text
                                    color='gray.400'
                                    mr='10px'
                                >
                                    Автор:
                                </Text>

                                {course.authorName}

                            </Flex>

                            {/* update date */}
                            <Flex
                                alignItems='center'
                            >
                                <Icon
                                    as={RepeatClockIcon}
                                    color='gray.400'
                                    w='24px'
                                    h='24px'
                                    mr='15px'
                                />

                                <Text
                                    color='gray.400'
                                >
                                    Последнее обновление:
                                </Text>

                                {courseUpdateDate}

                            </Flex>
                                
                        </Flex>

                        

                        <Flex
                            flexDir='column'
                            gap='40px'
                        >
                            {/* about course content */}
                            {courseBodies}

                            {/* learning plan */}
                            <LearningPlan/>
                        </Flex>
                        

                        

                        {/* author */}
                        <Flex
                            flexDir='column'
                            my='40px'
                        >
                            <Flex
                                alignItems='center'
                                mb='30px'
                            >
                                <Image
                                    src={faker.image.people()}
                                    w='150px'
                                    h='150px'
                                    borderRadius='50%'
                                    alt='author'
                                    mr='25px'
                                />
                                <Heading
                                    as='h3'
                                    fontSize='24px'
                                >
                                    {course.authorName}
                                </Heading>
                            </Flex>

                            <CourseBody 
                                body={{
                                    isAction: false,
                                    position: 10,
                                    title: "",
                                    bodyStyles: { 
                                        border: 'none',
                                        bg: 'inherit',
                                        padding: '0px'
                                    },
                                    items: course.aboutAuthor.items
                                }} 
                                courseId={course.courseId}
                            />

                        </Flex>

                        {/* return you money */}

                        <Box
                            bg={colors.greenGradient}
                            borderRadius='25px'
                            p='45px 75px 45px 35px'
                            my='30px'
                            position='relative'
                        >
                            <Image
                                src={shieldIcon}
                                position='absolute'
                                w='40px'
                                h='40px'
                                left='calc(100% - 65px)'
                                top='35px'
                            />

                            <Heading as='h5' fontSize='22px'>
                                Вернем деньги, если курс не подойдет
                            </Heading>

                            <Text
                                mt='20px'
                            >
                                Вы ничем не рискуете. Если курс не оправдает ваши ожидания 
                                в первые два дня после покупки, мы вернем полную стоимость 
                                на карту без выяснения причин. Просто напишите в поддержку 
                                на hello@edston.com
                            </Text>

                        </Box>

                        <Heading
                            as='h3'
                            fontSize='24px'
                            my='20px'
                        >
                            Ответы на часто задаваемые вопросы:
                        </Heading>

                        <FAQ/>

                    </Flex>

                </Container>

            </Box>

            <Footer/>

            
        </Box>
    )
}

export default CourseBrandPage