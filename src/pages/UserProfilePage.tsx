import { Avatar, Box, Container, Flex } from "@chakra-ui/react"
import { useEffect, useRef, useState } from "react"
import Header from "../components/Header/Header"
import Footer from "../sections/Footer/Footer"
import { IHandleUpdateUser, IUpdateUserInitial } from "../types/user"
import UpdateProfileForm from "../components/Forms/UpdateProfileForm"
import CallToActionImage, { getCallToActionImageData } from "../sections/Actions/CallToActionImage"
import { useAppSelector } from "../hooks/redux"



const UserProfilePage = (): JSX.Element => {

    const {user} = useAppSelector(state => state.user)

    const initialState: IUpdateUserInitial = {
        email: user.email,
        name: user.name,
        surname: user.surname
    }

    const [headingHeight, setHeadingHeight] = useState(0)

    const [resMessage, setResMessage] = useState<string>("")

    const ref = useRef(null)

    useEffect(() => {
        // @ts-ignore
        setHeadingHeight(ref.current.clientHeight)
        
    })

    const onUpdate = ({ isSuccessUpdate, formValue }: IHandleUpdateUser) => {
        if (isSuccessUpdate) {
            console.log('success update')
        }
    }

    return (
        <Box
            // height='100%'
            display='flex'
            flexDir='column'
        >
            <Box ref={ref}>
                <Header />
            </Box>
                
            <Box
                // flex='1 0'
            >
                <Container maxW={{base: 'calc(100% - 14px)', md: '100%', xl: '1240px'}} h='100%' >

                    <Flex
                        // h='100%'
                        mt='4rem'
                        justifyContent='stretch'
                        border='1px solid'
                        borderColor='gray.300'
                    >
                        
                            <Flex
                                h='100%'
                                p='30px'
                                pt='4rem'
                                
                                flex='1 1 30%'
                                maxW='30%'
                                justifyContent='center'
                            >
                                <Avatar size='2xl'src='https://bit.ly/broken-link' />
                                
                            </Flex>


                            <Box
                                h='100%'
                                p='30px'
                                borderLeft='1px solid'
                                borderColor='gray.300'
                                flex='1 1'
                            >
                                <UpdateProfileForm 
                                    handle={onUpdate} 
                                    resMessage={resMessage} 
                                    initialState={initialState}
                                />
                                
                            </Box>

                    </Flex>

                    

                </Container>

                <Box my='60px'>
                        <CallToActionImage props={getCallToActionImageData()}/>
                    </Box>

            </Box>

            <Footer/>

            
        </Box>
    )
}

export default UserProfilePage