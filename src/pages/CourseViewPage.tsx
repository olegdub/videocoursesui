import { Box, Container, Flex, Heading, Icon, Image, Spinner, Text } from "@chakra-ui/react"
import { useEffect, useRef, useState } from "react"
import Header from "../components/Header/Header"
import Footer from "../sections/Footer/Footer"
import { separator } from "../data/courses"
import { useAppDispach, useAppSelector } from "../hooks/redux"
import { useLocation } from "react-router-dom"
import { getCourseBrand } from "../redux/actions/courseBrandAction"



const CourseViewPage = (): JSX.Element => {

    const [headingHeight, setHeadingHeight] = useState(0)
    const dispatch = useAppDispach()
    const { course } = useAppSelector(state => state.courseBrand)

    const courseId = useLocation().pathname.split('/').pop()

    useEffect(() => {
        dispatch(getCourseBrand(Number(courseId)))
    }, [courseId])

    const ref = useRef(null)

    useEffect(() => {
        // @ts-ignore
        setHeadingHeight(ref.current.clientHeight)
        console.log(headingHeight)
        
      })

    if( course === undefined) return (
        <Box
            height='100%'
            display='flex'
            flexDir='column'
        >
            <Box ref={ref}>
                <Header />
            </Box>
                
            <Box
                flex='1 0'
            >
                <Container 
                    maxW={{base: 'calc(100% - 14px)', md: '100%', xl: '660px'}}
                    mt='60px'
                >
                    <Spinner/>
                </Container>
                </Box>

            <Footer/>
        </Box>
    )
    return (
        <Box
            height='100%'
            display='flex'
            flexDir='column'
        >
            <Box ref={ref}>
                <Header />
            </Box>
                
            <Box
                flex='1 0'
            >
                <Container 
                    maxW={{base: 'calc(100% - 14px)', md: '100%', xl: '660px'}}
                    mt='60px'
                >

                    {/* player */}
                    <Flex
                        flexDir='column'
                    >
                         <Box
                            overflow='hidden'
                            borderRadius='25px'
                        >
                            <div style={{"position": "relative", "paddingTop": "56.25%", "width": "100%"}}>
                                <iframe 
                                src={course.preview.split(separator)[1]}
                                allow="autoplay; fullscreen; picture-in-picture; encrypted-media;" 
                                frameBorder="0" allowFullScreen style={{"position": "absolute", "width": "100%", "height": "100%", "top": "0", "left": "0"}}/>
                            </div>
                        </Box>

                        <Heading
                            as='h2' fontSize='32'
                        >
                            
                        </Heading>


                    </Flex>

                </Container>

            </Box>

            <Footer/>

            
        </Box>
    )
}

export default CourseViewPage