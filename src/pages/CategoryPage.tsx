import { Box, Container, Flex, Grid, GridItem, Heading } from "@chakra-ui/react"
import { useEffect, useRef, useState } from "react"
import Header from "../components/Header/Header"
import { ISimpleMenuItem } from "../components/Menu/SimpeMenuItem"
import SimpleMenu from "../components/Menu/SimpleMenu"
import CallToActionImage, { getCallToActionImageData } from "../sections/Actions/CallToActionImage"
import Footer from "../sections/Footer/Footer"
import { useAppDispach, useAppSelector } from "../hooks/redux"
import { getCategoryDetail } from "../redux/actions/categoriesAction"
import { setCourseLink, transformReduxCategories } from "../utils.pages"
import { useLocation } from "react-router-dom"
import CourseCard from "../components/CourseCard/Card"
import { ICategoryDetail } from "../types/catalog"



const CategoryPage = (): JSX.Element => {

    const dispatch = useAppDispach()
    let location = useLocation()

    const { isAuthenticated } = useAppSelector(state => state.auth)
    const { category, loading, error} = useAppSelector(state => state.categoryDetail)

    const [headingHeight, setHeadingHeight] = useState(0)

    useEffect(()=> {
        
        let a = location.pathname.split('/')
        const categoryName = a[a.length - 1]
        console.log(categoryName)
        dispatch(getCategoryDetail(categoryName))
    }, [])

    useEffect(() => {
        console.log(category)
    }, [category])

    const ref = useRef(null)

    useEffect(() => {
        // @ts-ignore
        setHeadingHeight(ref.current.clientHeight)
        console.log(headingHeight)
        
      })

    const courses = category.courses.map( cd => {
        const course = setCourseLink(cd, false)
        return (
            <GridItem
                key={`course-${cd.id}`}
            >
                <CourseCard course={course}/>
            </GridItem>
        )
    })

    

    return (
        <Box
            height='100%'
            display='flex'
            flexDir='column'
        >
            <Box ref={ref}>
                <Header />
            </Box>

            <Box
                flex='1 0'
            >
                <Container maxW={{base: 'calc(100% - 14px)', md: '100%', xl: '1240px'}}>

                    <Flex
                        flexDir='column'
                        my='30px'
                    >
                        <Heading as='h2' fontSize='26' mb='20px'>
                            Курсы "{category.categoryName}"
                        </Heading>

                    </Flex>

                    <Box my='60px'>
                        <CallToActionImage props={getCallToActionImageData()}/>
                    </Box>

                    <Box
                        my='30px'
                    >

                        <Grid 
                            templateColumns='repeat(5, 1fr)'
                            gap='25px'
                        >
                            {courses}
                        </Grid>
                    </Box>

                </Container>

            </Box>

            <Footer/>

            
        </Box>
    )
}

export default CategoryPage