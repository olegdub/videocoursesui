import { Box, Container, Flex } from "@chakra-ui/react"
import { useEffect, useRef, useState } from "react"
import Header from "../components/Header/Header"
import Footer from "../sections/Footer/Footer"
import Error404 from "../components/Result/Error404"



const NotFoundPage = (): JSX.Element => {

    const [headingHeight, setHeadingHeight] = useState(0)

    const ref = useRef(null)

    useEffect(() => {
        // @ts-ignore
        setHeadingHeight(ref.current.clientHeight)
        
      })

    return (
        <Box
            height='100%'
            display='flex'
            flexDir='column'
        >
            <Box ref={ref}>
                <Header />
            </Box>
                
            <Box
                flex='1 0'
            >
                <Container maxW={{base: 'calc(100% - 14px)', md: '100%', xl: '1240px'}} h='100%' >

                    <Flex
                        flexDir='column'
                        justifyContent='center'
                        height='100%'
                    >
                        <Error404/>
                    </Flex>

                </Container>

            </Box>

            <Footer/>

            
        </Box>
    )
}

export default NotFoundPage