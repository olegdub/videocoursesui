import { Box, Button, Checkbox, CheckboxGroup, Container, Flex, FormControl, FormHelperText, FormLabel, Heading, Icon, Input, NumberDecrementStepper, NumberIncrementStepper, NumberInput, NumberInputField, NumberInputStepper, Popover, PopoverArrow, PopoverBody, PopoverCloseButton, PopoverContent, PopoverHeader, PopoverTrigger, SimpleGrid, Spinner, Text, Textarea } from "@chakra-ui/react"
import { FC, PropsWithChildren, useEffect, useRef, useState } from "react"
import Header from "../../components/Header/Header"
import Footer from "../../sections/Footer/Footer"
import { FiSave } from "react-icons/fi"
import MyPopover from "../../components/MyPopover/MyPopover"
import { DeleteIcon, InfoIcon } from "@chakra-ui/icons"
import { colors } from "../../theme"
import { useAppDispach, useAppSelector } from "../../hooks/redux"
import { getCategories } from "../../redux/actions/categoriesAction"
import FileUploader from "../../components/FormsComponents/FileUploader"
import CreateCourseBase, { CourseBaseFormValue } from "../../components/Forms/admin/CreateCourseBase"
import { IFormHandle } from "../../types/forms"
import CreateCourseDescription, { CourseDescription, ICourseDescriptionFormValue } from "../../components/Forms/admin/CreateCourseDescription"
import { DescriptionVariants, LessonVariants } from "../../data/createCourse"
import CreateCourseModule, { CourseModule, ICourseModuleFormValue } from "../../components/Forms/admin/CreateCourseModule"
import CreateCourseAboutAuthor, { CourseAuthorFormValue } from "../../components/Forms/admin/CreateCourseAboutAuthor"
import CreateCourseLesson, { CourseLesson, ICourseLessonFormValue } from "../../components/Forms/admin/CreateCourseLesson"
import { ICourseState, IDescription, IModuleLesson, IModule, ICreateCourseData, IAddCourseCategory, IAddCourseTextBlock, IAddCourseModule, ICreateCourseLesson, IAddLessonVideo } from "../../types/admin"
import { FaBook, FaBookOpen } from "react-icons/fa"
import StatsCard from "../../components/Dashboard/StatsCards"
import { createCourseFull } from "../../redux/actions/createCourseAction"



const getDefaultDescriptions = (): IDescription[] => {
    const descriptions: IDescription[] = []
    for (let i = 0; i < 2; i++) {
        descriptions.push(
            addDescription({order:i, id:i})
        )
        
    }

    return descriptions
}

const getDefaultModules = (): IModule[] => {
    const modules: IModule[] = []
    for (let i = 0; i < 1; i++) {
        const newModule = addModule({order: i, id: i, blockCount: 1})
        newModule.element.lessons = [addLesson({order: 0, id: 0, blockCount: 1, moduleId: i})]
        modules.push(newModule)
        
    }
    return modules
}

const addDescription = ({order, id}: { order: number, id: number}): IDescription => {
    return (
        {
            id: id,
            element: {
                variant: DescriptionVariants.base,
                order: order,
                initValues: {
                    title: '',
                    description: ''
                }
            },
            formValue: {
                title: '',
                description: ''
            }
        }
    )
}

const addModule = ({order, id, blockCount}: {order: number, id: number, blockCount: number}): IModule => {
    return (
        {
            id: id,
            element: {
                lessons: [addLesson({order: 0, id: 0, blockCount: 1, moduleId: id})],
                order: order,
                initValues: {
                    title: '',
                    description: ''
                },
                removeModuleHandler: ()=> {},
                addLessonHandler: ()=> {},
                modulesCount: blockCount
            },
            formValue: {
                title: '',
                description: ''
            }
        }
    )
}

const addLesson = ({order, id, blockCount, moduleId}: {order: number, id: number, blockCount: number, moduleId: number}): IModuleLesson => {
    return (
        {
            id: id,
            element: {
                variant: LessonVariants.video,
                order: order,
                initValues: {
                    title: '',
                    description: '',
                    video: null,
                    textContent: ''
                },
                lessonsCount: blockCount,
                parentModule: moduleId
            },
            formValue: {
                title: '',
                description: '',
                video: null,
                textContent: ''
            }
        }
    )
}

const getDefaultState = ():ICourseState => {

    return ({
        descriptions: getDefaultDescriptions(),
        modules: getDefaultModules()
    })
}

const AddCoursePage = (): JSX.Element => {

    const dispatch = useAppDispach()

    const { loading: sendingCourse, error } = useAppSelector(state => state.createCourse)

    const { categories, loading, error: categoriesError } = useAppSelector(state => state.categories)

    const [courseState, setCourseState] = useState<ICourseState>({descriptions: [], modules: []})

    const [ courseData, setCourseData ] = useState<CourseBaseFormValue>({
        title: '',
        description: '',
        preview: null, // (image)
        intro: null, //  (video)
        courseVideoDuration: 0,
        categories: []
    })

    useEffect( () => {
        console.log('sending course', sendingCourse)
    }, [sendingCourse])

    useEffect(() => {
        if(categories.length === 0 && !loading) {
            dispatch(getCategories()) 
        }   
    }, [categories, loading, dispatch])

    useEffect(() => {
        setCourseState(getDefaultState())
    }, [])

    const saveBase = ({isSuccess, formValue}:IFormHandle<CourseBaseFormValue>) => {
        // save course in future
        setCourseData(formValue)
        dispatch(getCategories())
    }

    const saveAuthor = ({isSuccess, formValue}:IFormHandle<CourseAuthorFormValue>) => {
        // save course in future
        dispatch(getCategories())
    }

    const createCourseDescriptionForms = (): JSX.Element[] => {
        if (courseState.descriptions.length === 0) return []
    
        return courseState.descriptions.map( d => {
    
            const saveDescription = ({isSuccess, formValue}:IFormHandle<ICourseDescriptionFormValue>) => {
                // save course in future
                const newState: ICourseState = courseState
                newState.descriptions = courseState.descriptions.map( nd => {
                    if (nd.id !== d.id) return nd
                    return  {
                        ...d,
                        formValue: formValue
                    }
                })
                setCourseState(newState)
                dispatch(getCategories())
                console.log(`block-${d.element.order}`, formValue)
            }
    
            return (
                <Box
                    my='30px'
                    w='100%'
                    key={d.id}
                >
                    <CreateCourseDescription
                        form={{formHandle: saveDescription, resMessage: error}} 
                        props={{
                            variant: d.element.variant,
                            initValues: d.formValue,
                            order: d.element.order
                        }}
                    >
                        <Button
                            variant='solid' 
                            colorScheme='blue'
                            borderRadius='25px'
                            m='20px 40px 40px 0'
                            p='10px 30px'
                            textAlign='center'
                            color='White'
                            bg='red.400'
                            _hover={{bg: 'red.200'}}
                            w='230px'
                            alignSelf='flex-end'
                            display='flex'
                            justifyContent='space-between'
                            onClick={()=> {removeDescriptionHandler(d.id)}}
                        >
                            <DeleteIcon w='18px' h='18px'/>Удалить блок
                        </Button>
                    </CreateCourseDescription>
                </Box>
                
            )
        })
    }

    const addNewDescriptionHandler = () => {
        let lastIndex: { order: number, id: number} = { order: 0, id: 0}
        const newState: ICourseState = {
            descriptions: courseState.descriptions.map(d => {
                if (d.id >= lastIndex.id) lastIndex.id = d.id + 1
                if (d.element.order >= lastIndex.order) lastIndex.order = d.element.order + 1

                return d
            }),
            modules: courseState.modules
        }

        newState.descriptions.push(addDescription(lastIndex))

        setCourseState(newState)
    }

    const removeDescriptionHandler = (id: number) => {
        const newState: ICourseState = {
            descriptions: courseState.descriptions.filter(d => d.id !== id),
            modules: courseState.modules
        }

        setCourseState(newState)
    }

    const addNewModuleHandler = () => {
        let lastIndex: { order: number, id: number} = { order: 0, id: 0}
        const newState: ICourseState = {
            modules: courseState.modules.map(m => {
                if (m.id >= lastIndex.id) lastIndex.id = m.id + 1
                if (m.element.order >= lastIndex.order) lastIndex.order = m.element.order + 1

                return m
            }),
            descriptions: courseState.descriptions
        }

        newState.modules.push(addModule({...lastIndex, blockCount: courseState.modules.length + 1}))

        setCourseState(newState)
    }

    const removeModuleHandler = (id: number) => {
        let order = 0
        const newState: ICourseState = {
            modules: courseState.modules
                .filter(m => m.id !== id)
                .map( m => {
                    const element = m.element
                    element.order = order
                    order += 1
                    return {...m, element: element}
                }),
            descriptions: courseState.descriptions
        }

        setCourseState(newState)
    }

    const removeLessonHandler = (moduleId: number, lessonId: number) => {
        const parentModule: IModule = courseState.modules.filter(m => m.id === moduleId)[0]
        let order = 0
        parentModule.element.lessons = parentModule.element.lessons
            .filter( l => l.id !== lessonId)
            .map( l => {
                const element = l.element
                element.order = order
                order += 1
                return {...l, element: element}
            })

        const newState: ICourseState = {
            modules: courseState.modules.map( m => {
                if (m.id === moduleId) return parentModule
                return m
            }),
            descriptions: courseState.descriptions
        }

        setCourseState(newState)
    }

    const addNewLessonHandler = (moduleId: number) => {
        let lastIndex: { order: number, id: number} = { order: 0, id: 0}
        const parentModule: IModule = courseState.modules.filter(m => m.id === moduleId)[0]
        parentModule.element.lessons.forEach( l => {
            if (l.id >= lastIndex.id) lastIndex.id = l.id + 1
            if (l.element.order >= lastIndex.order) lastIndex.order = l.element.order + 1
        })
        parentModule.element.lessons.push(addLesson({
            ...lastIndex, 
            blockCount: parentModule.element.lessons.length + 1,
            moduleId: parentModule.id
        }))
        const newState: ICourseState = {
            modules: courseState.modules.map(m => {
                if (m.id === moduleId) return parentModule
                return m
            }),
            descriptions: courseState.descriptions
        }

        setCourseState(newState)
    }

    const createModuleLessonForms = (module: IModule): JSX.Element[] => {
        if (module.element.lessons.length === 0) return []

        return module.element.lessons.map ( l => {
            const saveLesson = ({isSuccess, formValue}:IFormHandle<ICourseLessonFormValue>) => {
                const newState: ICourseState = courseState
                newState.modules = courseState.modules.map( om => {
                    if (om.id !== module.id) return om
                    module.element.lessons =  module.element.lessons.map( ol => {
                        if (ol.id !== l.id) return ol
                        return {
                            ...l,
                            formValue: formValue
                        }
                    })
                    return module
                })
                setCourseState(newState)
                dispatch(getCategories())
                console.log(`lesson-${l.element.order}`, formValue)
                
            }

            return (
                <Box
                    my='30px'
                    w='100%'
                    key={l.id}
                >
                    <CreateCourseLesson
                        form={{formHandle: saveLesson, resMessage: error}} 
                        props={{
                            variant: l.element.variant,
                            initValues: l.formValue,
                            order: l.element.order,
                            parentModule: module.element.order,
                            lessonsCount: module.element.lessons.length
                        }}
                    >
                        <Button
                            variant='solid' 
                            colorScheme='blue'
                            borderRadius='25px'
                            m='20px 40px 40px 0'
                            p='10px 30px'
                            textAlign='center'
                            color='White'
                            bg='red.400'
                            _hover={{bg: 'red.200'}}
                            w='230px'
                            alignSelf='flex-end'
                            display='flex'
                            justifyContent='space-between'
                            onClick={()=> {removeLessonHandler(module.id, l.id)}}
                        >
                            <DeleteIcon w='18px' h='18px'/>Удалить урок
                        </Button>
                    </CreateCourseLesson>
                </Box>
            )
        })
    }

    const createCourseModuleForms = (): JSX.Element[] => {
        if (courseState.modules.length === 0) return []
    
        return courseState.modules.map( m => {
    
            const saveModule = ({isSuccess, formValue}:IFormHandle<ICourseModuleFormValue>) => {
                // save course in future
                const newState: ICourseState = courseState
                newState.modules = courseState.modules.map( om => {
                    if (om.id !== m.id) return om
                    return {
                        ...m,
                        formValue: formValue
                    }
                })
                setCourseState(newState)
                dispatch(getCategories())
                console.log(`module-${m.element.order}`, formValue)
            }

            const lessons = createModuleLessonForms(m)

            const deleteModule = () => {
                removeModuleHandler(m.id)
            }

            const createLesson = () => {
                addNewLessonHandler(m.id)
            }
    
            return (
                <Box
                    my='30px'
                    w='100%'
                    key={m.id}
                >
                    <CreateCourseModule
                        form={{formHandle: saveModule, resMessage: error}} 
                        props={{
                            lessons: m.element.lessons,
                            initValues: m.formValue,
                            order: m.element.order,
                            removeModuleHandler: deleteModule,
                            addLessonHandler: createLesson,
                            modulesCount: courseState.modules.length
                        }}
                    >

                        {lessons}

                    </CreateCourseModule>
                </Box>
                
            )
        })
    }

    
    const courseDescriptions = createCourseDescriptionForms()

    const courseModules = createCourseModuleForms()

    let lessonsCount = 0
    courseState.modules.forEach( m => {
        lessonsCount += m.element.lessons.length
    })

    const onCreateCourseClick = () => {
        console.log("yep!\n", courseState)
        dispatch(createCourseFull(courseState, courseData))
    }

    return (
        <Box
            height='100%'
            display='flex'
            flexDir='column'
            mb='30px'
        >
       
            <Box
                my='30px'
                w='100%'
            >

                <CreateCourseBase formHandle={saveBase} resMessage={error}/>

                

            </Box>
                {courseDescriptions}

            <Box
                alignSelf='center'
                mb='20px'
            >
                <Button
                    variant='solid' 
                    colorScheme='blue'
                    borderRadius='25px'
                    p='10px 30px'
                    textAlign='center'
                    fontSize='18px'
                    color='White'
                    bg='teal.400'
                    _hover={{bg: 'teal.200'}}
                    minH='48px'
                    minW='230px'
                    justifySelf='flex-end'
                    display='flex'
                    gap='20px'
                    justifyContent='space-between'
                    onClick={addNewDescriptionHandler}
                >
                    <Icon as={FaBook}/>
                    Добавить блок описания
                </Button>
            </Box>

            {courseModules}

            <Box
                alignSelf='center'
                mb='20px'
            >
                <Button
                    variant='solid' 
                    colorScheme='blue'
                    borderRadius='25px'
                    p='10px 30px'
                    fontSize='18px'
                    textAlign='center'
                    color='White'
                    bg='teal.400'
                    _hover={{bg: 'teal.200'}}
                    minH='48px'
                    minW='230px'
                    justifySelf='flex-end'
                    display='flex'
                    gap='20px'
                    justifyContent='space-between'
                    onClick={addNewModuleHandler}
                >
                    <Icon as={FaBookOpen}/>
                    Добавить модуль
                </Button>
            </Box>

            <Box
                my='30px'
                w='100%'
            >

                <CreateCourseAboutAuthor formHandle={saveAuthor} resMessage={error}/>
            </Box>

            <Box
                my='30px'
                w='100%'
                bg='white'
                borderRadius='25px'
                p='80px 30px'
            >
                <Flex
                    w='100%'
                    flexDir='column'
                    alignItems='center'
                    mb='40px'
                >
                    <Heading
                        mb='10px'
                    >
                        Превосходно!
                    </Heading>
                    <Text
                        color='#9389BE'
                        fontSize='18px'
                    >
                        Почти готово! Осталось отправить курс на модерацию. Не забудьте проверить данные.
                    </Text>
                </Flex>

                <SimpleGrid columns={{ base: 1, md: 3 }} spacing={{ base: 5, lg: 8 }}>
                    <StatsCard title={'Блоков описания'} stat={`${courseState.descriptions.length} blocks`} />
                    <StatsCard title={'Модулей'} stat={`${courseState.modules.length} modules`} />
                    <StatsCard title={'Уроков'} stat={`${lessonsCount} lessons`} />
                </SimpleGrid>

                <Flex
                    w='100%'
                    justifyContent='center'
                    mt='40px'
                >
                    <Button 
                        variant='solid' 
                        colorScheme='blue'
                        borderRadius='25px'
                        p='10px 30px'
                        w='300px'
                        h='48px'
                        fontSize='20px'
                        textAlign='center'
                        color='White'
                        bg='linear-gradient(94.23deg, #FDF59E -4.24%, #EE9EA4 50.22%, #9389BE 105.83%);'
                        _hover={{bg: 'gray.300'}}
                        onClick={onCreateCourseClick}
                    >
                        {sendingCourse ? (
                            <Spinner/>
                        ) : "Создать курс" }
                    </Button>
                </Flex>

                
                
            </Box>

            
            
            
            
        </Box>
    )
}

export default AddCoursePage