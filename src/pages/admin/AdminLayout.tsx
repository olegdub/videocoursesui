import { Box, Container, Flex } from "@chakra-ui/react"
import { useEffect, useRef, useState } from "react"
import Header from "../../components/Header/Header"
import { colors } from "../../theme"
import { Outlet } from "react-router-dom"



const AdminLayout = (): JSX.Element => {

    const [headingHeight, setHeadingHeight] = useState(0)

    const ref = useRef(null)

    useEffect(() => {
        // @ts-ignore
        setHeadingHeight(ref.current.clientHeight)
        
      })

    return (
        <Box
            height='100%'
            display='flex'
            flexDir='column'
            
        >
            <Box ref={ref}>
                <Header />
            </Box>
                
            <Box
                flex='1 0'
                bg='gray.200'
            >
                <Container maxW={{base: 'calc(100% - 14px)', md: '100%', xl: '1240px'}} h='100%' >

                    <Flex
                        flexDir='column'
                        justifyContent='center'
                        height='100%'
                    >
                        <Outlet/>
                    </Flex>

                </Container>

            </Box>

            
        </Box>
    )
}

export default AdminLayout