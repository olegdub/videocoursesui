import { Box, Container, Flex, Heading } from "@chakra-ui/react"
import { useEffect, useRef, useState } from "react"
import BigCard from "../components/CourseCard/BigCard"
import getCardsData from "../components/CourseCard/utils"
import Header from "../components/Header/Header"
import { ISimpleMenuItem } from "../components/Menu/SimpeMenuItem"
import SimpleMenu from "../components/Menu/SimpleMenu"
import CallToActionImage, { getCallToActionImageData } from "../sections/Actions/CallToActionImage"
import Footer from "../sections/Footer/Footer"
import PopularCourses from "../sections/PopularCourses/PopularCourses"
import { ICourseCard } from "../components/CourseCard/interfaces"
import { useAppDispach, useAppSelector } from "../hooks/redux"
import { getCategories } from "../redux/actions/categoriesAction"
import { transformReduxCategories } from "../utils.pages"
import { getBigCourse, getNewCourses, getPopularCourses } from "../redux/actions/catalogActions"
import NewCourses from "../sections/PopularCourses/NewCourses"


const CatalogPage = (): JSX.Element => {

    const dispatch = useAppDispach()

    const { categories } = useAppSelector(state => state.categories)
    const { bigCourse } = useAppSelector(state => state.catalog)

    const [headingHeight, setHeadingHeight] = useState(0)

    const [menuCategories, setMenuCategories] = useState<ISimpleMenuItem[]>([])

    useEffect(()=> {
        dispatch(getCategories())
        dispatch(getPopularCourses())
        dispatch(getBigCourse())
        dispatch(getNewCourses())
    }, [])

    useEffect(() => {
        setMenuCategories(transformReduxCategories(categories))
        
    }, [categories])

    const ref = useRef(null)

    useEffect(() => {
        // @ts-ignore
        setHeadingHeight(ref.current.clientHeight)
        console.log(headingHeight)
        
      })

    const bigCourseCard = bigCourse !== null ? <BigCard course={bigCourse}/> : null

    return (
        <Box
            height='100%'
            display='flex'
            flexDir='column'
        >
            <Box ref={ref}>
                <Header />
            </Box>
                
            <Box
                flex='1 0'
            >
                <Container maxW={{base: 'calc(100% - 14px)', md: '100%', xl: '1240px'}}>

                    <Flex
                        flexDir='column'
                        my='60px'
                    >
                        <Heading as='h2' fontSize='26'>
                            menu heading
                        </Heading>

                        <SimpleMenu menuData={menuCategories}/>
                    </Flex>

                    <Box my='120px'>
                        <CallToActionImage props={getCallToActionImageData()}/>
                    </Box>

                    <PopularCourses/>


                    {bigCourseCard}

                    <NewCourses/>

                </Container>

            </Box>

            <Footer/>

            
        </Box>
    )
}

export default CatalogPage