import { Box, Container, Flex, Grid, GridItem, Heading, SimpleGrid } from "@chakra-ui/react"
import { faker } from "@faker-js/faker"
import { useEffect, useMemo, useRef, useState } from "react"
import BigCard from "../components/CourseCard/BigCard"
import { ICourseCard } from "../components/CourseCard/interfaces"
import getCardsData from "../components/CourseCard/utils"
import Header from "../components/Header/Header"
import { ISimpleMenuItem } from "../components/Menu/SimpeMenuItem"
import SimpleMenu from "../components/Menu/SimpleMenu"
import CallToActionImage, { getCallToActionImageData } from "../sections/Actions/CallToActionImage"
import Footer from "../sections/Footer/Footer"
import PopularCourses from "../sections/PopularCourses/PopularCourses"
import CourseCard from "../components/CourseCard/Card"
import CoursesByCategories from "../sections/PopularCourses/CoursesByCategories"
import { useAppDispach, useAppSelector } from "../hooks/redux"
import { getBigCourse, getNewCourses, getPopularCourses } from "../redux/actions/catalogActions"
import NewCourses from "../sections/PopularCourses/NewCourses"
import { apiUrls, sitePaths } from "../axios"
import { getMyCourses } from "../redux/actions/userCoursesAction"
import { setCourseLink } from "../utils.pages"


const UserCourses = (): JSX.Element => {

    const [headingHeight, setHeadingHeight] = useState(0)

    const dispatch = useAppDispach()
    const { bigCourse } = useAppSelector(state => state.catalog)
    const { myCourses } = useAppSelector(state => state.userCourses)

    useEffect(() => {
        // dispatch(getCategories())
        dispatch(getPopularCourses())
        dispatch(getBigCourse())
        dispatch(getNewCourses())
        dispatch(getMyCourses())
    }, [])

    const ref = useRef(null)

    useEffect(() => {
        // @ts-ignore
        setHeadingHeight(ref.current.clientHeight)
        
    })

    let courses: JSX.Element[] = useMemo<JSX.Element[]>(
        () => myCourses.map(cd => {
            const course = setCourseLink(cd, true)
            return (
                <GridItem key={`course-${cd.id}`} h='100%'>
                    <CourseCard  course={course}/>
                </GridItem>
            )
        }
            
        ), 
    [myCourses])

    console.log(courses)

    if (myCourses.length === 0) {
        courses = []
    }

    const bigCourseCard = bigCourse !== null ? <BigCard course={bigCourse}/> : null

    return (
        <Box
            height='100%'
            display='flex'
            flexDir='column'
        >
            <Box ref={ref}>
                <Header />
            </Box>
                
            <Box
                flex='1 0'
            >
                <Container maxW={{base: 'calc(100% - 14px)', md: '100%', xl: '1240px'}}>


                    <PopularCourses forUser={false}/>

                    <Heading
                        as='h2'
                        fontSize='24px'
                        my='30px'
                    >
                        Мои курсы
                    </Heading>

                    <SimpleGrid 
                        templateColumns={`repeat(${4}, 1fr)`} 
                        templateRows='1fr'
                        gap={6} 
                    > 
                        {courses}
                    </SimpleGrid>


                    <Box
                        my='60px'
                    >
                        {bigCourseCard}
                    </Box>

                    <CoursesByCategories forUser={false}/>

                    <NewCourses forUser={false}/>

                </Container>

            </Box>

            <Footer/>

            
        </Box>
    )
}

export default UserCourses