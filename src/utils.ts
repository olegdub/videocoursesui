

export function randomNumberInRange(min: number, max: number):number {
    // 👇️ get number between min (inclusive) and max (inclusive)
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

export function getHash(input: string): number{
    var hash = 0, len = input.length;
    for (var i = 0; i < len; i++) {
      hash  = ((hash << 5) - hash) + input.charCodeAt(i);
      hash |= 0; // to 32bit integer
    }
    return hash;
  }

export function removeLastDirectoryPartOf(url: string)
{
    var the_arr = url.split('/');
    the_arr.pop();
    return( the_arr.join('/') );
}