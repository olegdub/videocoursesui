import { Box, ChakraProvider } from '@chakra-ui/react'
import { Route, Routes } from 'react-router-dom'
import Home from './pages/Home'
import CatalogPage from './pages/Catalog'
import CategoryPage from './pages/CategoryPage'
import CourseBrandPage from './pages/CourseBrandPage'
import Login from './components/Auth/Login'
import { sitePaths } from './axios'
import Error404 from './components/Result/Error404'
import NotFoundPage from './pages/NotFoundPage'
import InWork from './components/Result/InWork'
import InWorkPage from './pages/InWorkPage'
import ProtectedRoutes from './components/Route/ProtectedRoutes'
import LogOut from './components/Auth/Logout'
import UserProfilePage from './pages/UserProfilePage'
import UserCourses from './pages/UserCourses'
import Registration from './components/Auth/Registration'
import AddCoursePage from './pages/admin/AddCoursePage'
import AdminLayout from './pages/admin/AdminLayout'
import CourseViewPage from './pages/CourseViewPage'

function App() {  

  return (
    <Box w='100%' h='100%'>
      <Routes>
        <Route 
          path={sitePaths.home}
          element={<Home/>}
        />
        <Route 
          path={sitePaths.catalog}
          element={<CatalogPage/>}
        />
        <Route 
          path={sitePaths.categoryPage}
          element={<CategoryPage/>}
        />
        <Route 
          path={sitePaths.courseBrand}
          element={<CourseBrandPage/>}
        />
        <Route 
          path={sitePaths.login}
          element={<Login/>}
        />

        <Route 
          path={sitePaths.register}
          element={<Registration/>}
        />

        <Route element={<ProtectedRoutes/>}>
          <Route 
            path={sitePaths.userProfile}
            element={<UserProfilePage/>}
          />
          <Route 
            path={sitePaths.userCourses}
            element={<UserCourses/>}
          />

        <Route 
          path={sitePaths.courseView}
          element={<CourseViewPage/>}
        />

          {/* TODO: only for admin */}
          <Route 
            element={<AdminLayout/>}
          >
            <Route 
              path={sitePaths.createCourse}
              element={<AddCoursePage/>}
            />
          </Route>
          {/* TODO: only for admin */}

        </Route>

        <Route 
          path={sitePaths.buyPage}
          element={<InWorkPage/>}
        />
        <Route 
          path={sitePaths.forgotPassword}
          element={<InWorkPage/>}
        />
        <Route 
          path={sitePaths.logout}
          element={<LogOut/>}
        />

        <Route
          path='*'
          element={<NotFoundPage/>}
        />
      </Routes>
    </Box>
  )
}

export default App