// catalog 

import { sitePaths } from "./axios"
import { ICourseCard } from "./components/CourseCard/interfaces"
import { ISimpleMenuItem } from "./components/Menu/SimpeMenuItem"
import { ICategoryInList } from "./types/catalog"
import { removeLastDirectoryPartOf } from "./utils"

export const transformReduxCategories = (categories: ICategoryInList[]): ISimpleMenuItem[] => {

    const handleClick = (currentItemid: number): void => {
        console.log(currentItemid)
    }
    const data: ISimpleMenuItem[] = categories.map( category => (
        {
            id: category.id,
            text: category.categoryName,
            link: category.link,
            handleClick
        }
    ))
  
    return data
  }


export const setCourseLink = (course: ICourseCard, isAuth: boolean) => {
    const courseLink = isAuth ? 
        removeLastDirectoryPartOf(sitePaths.courseView) : 
        removeLastDirectoryPartOf(sitePaths.courseBrand)
    return {...course, link: `${courseLink}/${course.id}`}
  }