export interface ICategoriesListResponse {
    cat_name: string,
    description: string
}

export interface ICategoryDetailResponse
    extends ICategoriesListResponse {
        courses: ICoursePreviewResponse[]
}

// export interface ICategoriesListResponse {
//         data: ICategoriesList[]
// }

export interface ICoursePreviewResponse {
    course: ICoursePreview
}

export interface IMyCoursesResponse {
    data: ICoursePreview[]
}

export interface ICoursePreview {
    course_id: number,
    title: string,
    preview_url: string,
    author: IAuthorResponse,
    prod: ICourseProductData,
}

export interface IUserR {
    user_id: number,
    username: string,
    email: string,
    fname: string,
    lname: string,
    mname: string
}

export interface IUserResponse {
    status: number,
    data: IUserR
}

export interface IAuthorResponse {
    author_id: number,
    work_experience: string,
    description: string,
    user: IUserR
}

export interface ICourseProductData {
    prod_id: number,
    title: string,
    description: string,
    origin_price: string,
    price: string
}

export interface ICourseCategory {
    is_main: boolean,
    category: ICategoriesListResponse
}

export interface ICourseLesson {
    lesson_id: number,
    title: string,
    type: string,
    description: string,
    order: number
}

export interface ICourseModule {
    learn_block_id: number,
    order: number,
    title: string,
    description: string,
    lessons: ICourseLesson[]
}

export interface ICourseTextBlock {
    text_block_id: number,
    order: number,
    text: string,
    type: string,
    childes: ICourseTextBlock[]

}

export interface ICourseDemo {
    course_id: number,
    title: string,
    preview_url: string,
    author: IAuthorResponse,
    prod: ICourseProductData,
    cat: ICourseCategory,
    learn_blocks: ICourseModule[],
    text_blocks: {block: ICourseTextBlock}[]
}

export interface ICourseDemoResponse {
    status: number,
    data: ICourseDemo
}

export interface ICourseCreateResponse {
    data: {
        course_id: number,
        title: string,
        preview_url: string,
        author: number,
        prod: {
            prod_id: number,
            title: null,
            description: null,
            origin_price: string,
            price: string
        }
    }
}

export interface ICourseCreateModuleResponse {
    data: {
        learn_block_id: number,
        order: number,
        title: string,
        description: string,
        lessons: any[]
      }
}

export interface ICourseCreateLessonResponse {
    data: {
        lesson_id: number,
        order: number,
        title: string,
        description: string,
        type: string | null
      }
}

export interface IVideoUploadKinescopeResponse {
    data: {
        crated_at: string,
        description: string,
        embed_link: string,
        folder_id: string,
        id: string,
        play_link: string,
        player_link: string,
        poster: string,
        project_id: string,
        title: string,
        type: string,
        version: number
    }
}
