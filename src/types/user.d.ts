export interface  IUser {
    id: number,
    email: string,
    name: string,
    surname: string, 
}

export interface IUpdateUser {
    name: string,
    surname: string
}

export interface IUpdateUserInitial extends IUpdateUser{
    email: string
}

export interface IHandleUpdateUser {
    isSuccessUpdate: boolean;
    formValue: IUpdateUser
}

export interface ICreateUser {
    email: string,
    name: string,
    surname: string,
    password: string
}