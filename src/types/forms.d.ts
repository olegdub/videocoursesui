export interface IFormHandle<Type> {
    isSuccess: boolean,
    formValue: Type
}

export interface IFormProps<Type> {
    formHandle: ({ isSuccess, formValue }: IFormHandle<Type>) => void;
    resMessage: string,
}