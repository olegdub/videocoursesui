
export interface ITokenData {
    access_token: string,
    refresh_token: string,
    expires_in: string
}

export interface IRegisterResponse {
    status: number,
    data: {
        refresh: string, 
        access: string
    }
}

export interface ILoginValue {
    email: string,
    password: string
}

export interface IHandleLogin {
    isSuccessLogin: boolean;
    formValue: ILoginValue
}



export interface IRegisterValue {
    username: string,
    email: string;
    password: string;
    re_password: string
}

export interface IHandleRegister {
    isSuccess: boolean;
    formValue: IRegisterValue
}