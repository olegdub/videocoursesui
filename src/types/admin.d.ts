import { CourseDescription, ICourseDescriptionFormValue } from "../components/Forms/admin/CreateCourseDescription";
import { CourseLesson, ICourseLessonFormValue } from "../components/Forms/admin/CreateCourseLesson";
import { CourseModule, ICourseModuleFormValue } from "../components/Forms/admin/CreateCourseModule";

export interface IModuleLesson {
    id: number,
    element: CourseLesson,
    formValue: ICourseLessonFormValue
}

export interface IModule {
    id: number,
    element: CourseModule,
    formValue: ICourseModuleFormValue
    
}

export interface IDescription {
    id: number,
    element: CourseDescription,
    formValue: ICourseDescriptionFormValue
}


export interface ICourseState  {
    descriptions: IDescription[],
    modules: IModule[]
}



export interface ICreateCourseData {
    title: string,
    preview_url: string,
    price: number
}

export interface IAddCourseCategory {
    courseId: number, 
    name: string, 
    isMain: number
}

export interface IAddCourseTextBlock {
    text: string,
    type: string,
    order: number,
}

export interface IAddCourseBlock {
    order: number,
    title: string,
    description: string,
    
}

export interface IAddCourseModule extends IAddCourseBlock {
    course: number
}

export interface ICreateCourseLesson extends IAddCourseBlock {
    learn_block: number
}

export interface IAddLessonVideo {
    video_id_related: null | string,
    url: null | string,
    title: string,
    description: string,
    length: string,
    type: string
}