import { ICourseCard } from "../components/CourseCard/interfaces"

export interface ICategoryInList {
    id: number,
    categoryName: string,
    link: string
}


export interface ICategoryDetail extends ICategoryInList{
    courses: ICourseCard[]
}
