import { Box, Heading } from "@chakra-ui/react"
import { faker } from "@faker-js/faker"
import { FC, useEffect, useMemo, useState } from "react"
import CourseCard from "../../components/CourseCard/Card"
import { ICourseCard } from "../../components/CourseCard/interfaces"
import getCardsData from "../../components/CourseCard/utils"
import CourseCarousel from "../../components/CourseCarousel/CourseCarousel"
import DragScroll from "../../components/Menu/DragScroll"
import { useAppSelector } from "../../hooks/redux"
import { setCourseLink } from "../../utils.pages"


// TO DO: 
// make component with children

interface CoursesByCategoriesProps {
    forUser?: boolean
}

const CoursesByCategories: FC<CoursesByCategoriesProps> = ({forUser}): JSX.Element => {

    if (forUser === undefined) forUser = false

    const { category } = useAppSelector(state => state.categoryDetail)

    useEffect(() => {
        // get by category
        setCardsData(category.courses)
    }, [category])

    const [cardsData, setCardsData ] = useState<ICourseCard[]>([])

    const courses = useMemo<JSX.Element[]>(
        () => cardsData.map(cd => {
            const course = setCourseLink(cd, false)
            return <CourseCard key={`course-${cd.id}`} course={course}/>
        }), 
    [cardsData])

    const settings = {
        currentSlide: 1,
        howSlidesShow: 5,
        slidesCount: courses.length,
        children: courses
    }

    const heading = 'Курсы на ProstoCourse'

    return (
        <Box
            w='100%'
            h='100%'
            display='flex'
            flexDir='column'
            p='40px 0'
        >
            <Heading as='h3' fontSize='24px' mb='30px'>
                {heading}
            </Heading>
            <DragScroll/>
            <CourseCarousel settings={settings}/>
        </Box>

    )
}

export default CoursesByCategories