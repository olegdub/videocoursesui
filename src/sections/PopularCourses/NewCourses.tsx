import { Box, Heading } from "@chakra-ui/react"
import { FC, useEffect, useMemo, useState } from "react"
import CourseCard from "../../components/CourseCard/Card"
import { ICourseCard } from "../../components/CourseCard/interfaces"
import CourseCarousel from "../../components/CourseCarousel/CourseCarousel"
import { useAppSelector } from "../../hooks/redux"

interface NewCoursesProps {
    forUser?: boolean
}


const NewCourses: FC<NewCoursesProps> = ({forUser}): JSX.Element => {

    const { newCourses } = useAppSelector(state => state.catalog)

    if (forUser === undefined) forUser = false

    useEffect(() => {
        // get by category
        setCardsData(newCourses)
    }, [newCourses])

    const [cardsData, setCardsData ] = useState<ICourseCard[]>([])

    const courses = useMemo<JSX.Element[]>(
        () => cardsData.map(cd => (
            <CourseCard key={`course-${cd.id}`} course={cd}/>
        )), 
    [cardsData])

    const settings = {
        currentSlide: 1,
        howSlidesShow: 5,
        slidesCount: courses.length,
        children: courses
    }

    const heading = "Новое на ProstoCourse"

    return (
        <Box
            w='100%'
            h='100%'
            display='flex'
            flexDir='column'
            p='40px 0'
        >
            <Heading as='h3' fontSize='24px' mb='30px'>
                {heading}
            </Heading>
            <CourseCarousel settings={settings}/>
        </Box>

    )
}

export default NewCourses