import { Box, Heading } from "@chakra-ui/react"
import { FC, useEffect, useMemo, useState } from "react"
import CourseCard from "../../components/CourseCard/Card"
import { ICourseCard } from "../../components/CourseCard/interfaces"
import CourseCarousel from "../../components/CourseCarousel/CourseCarousel"
import { useAppSelector } from "../../hooks/redux"
import { setCourseLink } from "../../utils.pages"

interface PopularCoursesProps {
    forUser?: boolean
}


const PopularCourses: FC<PopularCoursesProps> = ({forUser}): JSX.Element => {

    const { popular } = useAppSelector(state => state.catalog)

    if (forUser === undefined) forUser = false
    
    const courses = useMemo<JSX.Element[]>(
        () => popular.map(cd => {
            const course = setCourseLink(cd, false)
            return (
                <CourseCard key={`course-${cd.id}`} course={course}/>
            )
        }), 
    [popular])

    const settings = {
        currentSlide: 1,
        howSlidesShow: 5,
        slidesCount: courses.length,
        children: courses
    }

    const heading = "Популярные на ProstoCourse"

    return (
        <Box
            w='100%'
            h='100%'
            display='flex'
            flexDir='column'
            p='40px 0'
        >
            <Heading as='h3' fontSize='24px' mb='30px'>
                {heading}
            </Heading>
            <CourseCarousel settings={settings}/>
        </Box>

    )
}

export default PopularCourses