import { Accordion, Box, Divider, Flex, Heading, Text } from "@chakra-ui/react"
import { FC, useEffect, useState } from "react"
import MyAccordionItem from "../../components/Accordion/MyAccordion"
import { faker } from "@faker-js/faker"
import { useAppSelector } from "../../hooks/redux"


interface ILesson {
    id: number,
    title: string,
    type: string,
    description: string
}

interface IModule {
    id: number,
    title: string,
    description: string,
    order: number,
    lessons: ILesson[]
}

interface ILearningPlanProps {
    modules: IModule[]
}


const getLearningPlan = (): IModule[] => {

    const modules: IModule[] = []
    
    for (let i = 0; i < 4; i++) {

        const lessons: ILesson[] = []

        for (let j = 0; j < 7; j++) {
            lessons.push({
                id: i*10 +j,
                title: faker.company.name(),
                type: 'video',
                description: faker.lorem.lines(3)
            })
            
        }

        modules.push({
            id: i,
            order: i,
            title: faker.company.name(),
            description: faker.lorem.lines(3),
            lessons
        });
        
    }

    return modules
}



const LearningPlan = (): JSX.Element => {

    const { learnBlocks } = useAppSelector(state => state.courseBrand)

    // const [modules, setModules] = useState<IModule[]>([])

    // useEffect(() => {
    //     setModules(getLearningPlan())
    // }, [setModules])


    let lessonsCount = 0

    const planItems = learnBlocks.map( (m, mNumber) => {

        lessonsCount += m.lessons.length

        const lessons = m.lessons.map( (l, lNumber) => (
            <MyAccordionItem key={l.lesson_id} props={{
                title: `Урок ${mNumber}.${lNumber} ${l.title}`,
                text: l.description
            }}/>
        )) 
        
        
        return (
            <Box key={m.learn_block_id} >

                <Heading 
                    as='h4' 
                    fontSize='18px'
                >
                    {m.title}
                </Heading>

                <Text
                    my='10px'
                    fontSize='14px'
                    color='gray.400'
                >
                    {m.description}
                </Text>
                

                <Divider/>

                <Accordion defaultIndex={[0]} allowMultiple>
                    {lessons}
                </Accordion>

                <Divider/>

            </Box>
        )
}   ) 

    return (
        <Flex
            flexDir='column'
            gap='20px'
        >
            <Flex
                flexDir='column'
                gap='10px'
            >
                <Heading 
                    as='h2' 
                    fontSize='24px'
                >
                    Учебный план:
                </Heading>
                <Text>
                    {lessonsCount} lessons
                </Text>
            </Flex>

            {planItems}
            
        </Flex>
    )
}

export default LearningPlan